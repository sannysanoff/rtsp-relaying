/*
 *  Copyright(c), 2002 The GHC Team.
 */

#ifdef aix_HOST_OS
#define _LINUX_SOURCE_COMPAT
// Required to get CMSG_SPACE/CMSG_LEN macros.  See #265.
// Alternative is to #define COMPAT_43 and use the
// HAVE_STRUCT_MSGHDR_MSG_ACCRIGHTS code instead, but that means
// fiddling with the configure script too.
#endif

#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

#ifdef LINUX
#include <linux/socket.h>
#include <bits/sockaddr.h>
#endif
#include <netinet/in.h>


int setMulticastTTL(int sock, int ttl) {
  return setsockopt(sock, IPPROTO_IP,IP_MULTICAST_TTL,&ttl,sizeof ttl);
}

int joinMulticast(int sock, int destAddr, int srcAddr)
{
  struct ip_mreq        group;
  memset(&group, 0, sizeof (group));
  group.imr_multiaddr.s_addr = destAddr;
  group.imr_interface.s_addr = srcAddr;
  // printf ("join Multicast: %d %d %d\n",sock,destAddr,srcAddr);
  if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                 (char *)&group, sizeof(group)) < 0) {
	perror ("join Multicast: result = FAIL:\n");
	return 0;
  }
  // printf ("join Multicast: result = OK\n");
  return 1;
}

int leaveMulticast(int sock, int destAddr, int srcAddr)
{
  struct ip_mreq        group;
  memset(&group, 0, sizeof (group));
  group.imr_multiaddr.s_addr = destAddr;
  group.imr_interface.s_addr = srcAddr;
  // printf ("leave Multicast: %d %d %d\n",sock,destAddr,srcAddr);
  if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                 (char *)&group, sizeof(group)) < 0) {
	perror ("leave Multicast: result = FAIL:\n");
	return 0;
  }
  // printf ("join Multicast: result = OK\n");
  return 1;
}

int enableReusePort(int socket) {
#ifdef SO_REUSEPORT
	int one = -1;
// 	printf("enableReusePort: %d\n",socket);
	return setsockopt(socket, SOL_SOCKET, SO_REUSEPORT, &one, sizeof(one));
#else
	return 0;
#endif
}

void testFunction(int handle) {
	char buf[1500];
	// setsockopt(handle, SO_
    int flags;
    flags = fcntl(handle,F_GETFL,0);
    fcntl(handle, F_SETFL, flags & ~O_NONBLOCK);
	while (1) {
		int i = recvfrom(handle, buf, 1500, 0, NULL, 0);
		if (i == -1) {
		    printf("errno=%d ",errno);
		}
		// printf("."); fflush(stdout);
	}
}



