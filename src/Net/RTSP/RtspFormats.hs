{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE BangPatterns #-}
{-# OPTIONS -fexcess-precision #-}

module Net.RTSP.RtspFormats where

import Text.Regex
import Data.ByteString.Internal
import qualified Data.ByteString as BS
import qualified Control.Applicative as CA
-- import Debug.Trace
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Combinator
import Text.Parsec.Language ()
import Data.Foldable()
import Utils.SanUtils

import Net.RTSP.RtspTypes
import Debug.Trace


decodeRTSPRequest :: ByteString -> RTSPRequest
decodeRTSPRequest s =
        let p = do
                cmd <- traceval1 "CMD GOT: " CA.<$> (many1 (letter <|> char '_'))
                spaces
                url <- manyTill anyChar space
                spaces
                (string "RTSP/1.0" <|> string "HTTP/")
                return $ case filter (\(c,_) -> c == cmd) [
                                           ("OPTIONS",RTSPOptions),
                                           ("DESCRIBE",RTSPDescribe),
                                           ("POSITION",RTSPDummy),
                                           ("SETUP",RTSPSetup),
                                           ("PLAY",RTSPPlay),
                                           ("GET",HTMLGet),
                                           ("PAUSE",RTSPPause),
                                           ("TEARDOWN",RTSPTeardown),
                                           ("GET_PARAMETER",RTSPGetParameter)
                                           ] of
                        [(_,constr)] -> constr $ normalizeURL url
                        _ -> trace "invalid request!!" $ RTSPInvalidRequest $ "wrong cmd: "++cmd
        in
            let s' = if (s == (BS.pack $ map c2w "position\r")) then BS.pack $ map c2w "POSITION xxx RTSP/1.0" else s
            in case (runParser p () "location" s') of
                   Left err -> traceval1 ("runParserFailed("++(show s)++"): ") CA.<$> RTSPInvalidRequest $show err
                   Right val -> val
        where normalizeURL url = subRegex (mkRegex "%3F") url "?"

isValidRequest s = case decodeRTSPRequest s of
                        RTSPInvalidRequest _ -> False
                        _ -> True


