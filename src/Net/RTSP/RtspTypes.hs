module Net.RTSP.RtspTypes where

import System.IO
import System.Clock
import Data.Word
--import Data.ByteString.Lazy
import Data.IORef
import Foreign.Ptr
import Control.Concurrent.MVar
import Network.Socket
import Data.Queue.SimplestQueue
import CapturingServer.Storage

data StreamSource = HandleStreamSource Handle
                | StorageStreamSource ITSStorage (IORef ITSSegment)


instance Show StreamSource where
    show (HandleStreamSource ha) = "HandleStreamSource "++(show ha)
    show (StorageStreamSource stor _) = "StorageStreamSource(stor="++(show stor)++")"

data RTSPStreaming = RTSPStreaming {
    rssSeq :: !Word16,
    rssRtpTime :: !Word32,
    rssFileHandle :: StreamSource,        -- TS file handle
    rssStreamedPos :: !Integer,      -- last streamed block in file
    rssPacketCount :: !Int,
    rssStreamedPTS :: !Double,
    rssPacketSender :: PacketSender,
    rssStreamedPTSDelta :: !Double      -- last PTS delta
}


data SourceFileType = SourceFileTS | SourceFileITS deriving (Eq,Show)

data RTSPSession = RTSPSession {
    rsClientSockAddr :: SockAddr,
    rsServerSockAddr :: SockAddr,
    rsSocketHandle :: Handle,       -- UDP stream handle
    rsURL :: String,
    rsNumber :: Int,
    rsActive :: MVar Bool,
    rsUserKey :: Maybe String,
    rsSourceFileType :: SourceFileType,
    rsPlayState :: IORef PlayState,
    rsStreaming :: MVar RTSPStreaming
}

instance Show RTSPSession where
    show sess = "Session [userKey="++(show $ rsUserKey sess)++",url="++(rsURL sess)++"]"


type TimeFunction = RTSPSession -> PlayState -> IO Double
data PlayState =
        PSInitializing
        | PSPlaying Double          -- baseTime = currentTime - ITSTimestamp
        | PSPaused

data RTSPRequest = RTSPInvalidRequest String|
                    RTSPOptions String |
                    RTSPDescribe String |
                    RTSPSetup String |
                    RTSPPlay String |
                    HTMLGet String |
                    RTSPPause String |
                    RTSPDummy String |
                    RTSPGetParameter String |
                    RTSPTeardown String
                deriving (Show, Eq)

data PacketSender = PacketSender {
    psHandle :: Handle,
    psPackets :: MVar OutPackets,
    psSequence :: IORef Word16,
    psAlive :: IORef Bool
}

data ITSBlock = ITSBlock {
        itsTime :: TimeSpec,
        itsTS :: Ptr Word8      -- malloc'd
    }

data OutPacket = OutPacket {
    --opData :: [TSPacket],
    opData :: [(Ptr Word8, IO ())],
    opTimeInBase :: !Double             -- e.g. 3.3448039
}

-- showOutPacket op = "[@"++show (opTimeInBase op) ++" = " ++(Data.List.intercalate "," $ Prelude.map (show . tspFileOffset) (opData op)) ++ "]"

data OutPackets = OutPackets {
    opsLastAddedPacket :: !(Maybe OutPacket),
    opsPackets :: !(Que OutPacket),
    opsBaseTime :: !Double,             -- e.g 12387847874
    opsPaused :: !Bool
}

