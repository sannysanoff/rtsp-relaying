{-# OPTIONS -XForeignFunctionInterface -XBangPatterns -XExistentialQuantification -XScopedTypeVariables -XFlexibleContexts #-}
-----------------------------------------------------------------------------
--
-- Module      :  Main.Server
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module RelayingServer.Relaying where

import Network.Socket
import Network.BSD
import Control.Concurrent
import Control.Exception(bracket)
import Control.Monad.Loops
import Control.Monad
import Control.Applicative hiding (many, optional)
import Data.Word
-- import Data.ByteString.Internal
import Data.Maybe
import Data.IORef
import Data.Time
import Data.Int
import Text.URI
import System.IO
import System.IO.Error
-- import System.Posix.Types
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Array
import Foreign.Ptr
import System.IO.Unsafe
import System.Posix.Process
import System.Clock
import Data.Bits
import qualified Data.ByteString.Lazy as LBS
import qualified Control.Exception as CE
import System.Posix.Signals
import GHC.Exception
import System.Posix.IO
import Codec.MpegTS.Formats
import MPEG.MpegTypes
-- import Debug.Trace
import Text.Parsec hiding (Ok,label)
import Utils.SanUtils (endsWith)
import RelayingServer.Filter



{-# NOINLINE currentPID #-}
currentPID = unsafePerformIO $ getProcessID

{-# NOINLINE displayStatsVar #-}
displayStatsVar = unsafePerformIO $ newIORef $ False

{-# NOINLINE displayDebugVar #-}
displayDebugVar = unsafePerformIO $ newIORef False

defaultUserAgent = "Opera/9.60 (Linux 7402f0-ZZmod ; U; en) Presto/2.1.1"

fromJustX :: String -> Maybe a -> a
fromJustX _ (Just x) = x
fromJustX lbl Nothing = error $ "fromJustX: Nothing, label=" ++ lbl

swapBytes i =
        let lo = (i .&. 0xFF) `shiftL` 8
            hi = i `shiftR` 8
        in lo .|. hi


cast :: (Integral a, Integral b) => a -> b
cast = fromInteger . toInteger

enableReusePort sock = do
    let fd = cast $ fdSocket sock
    enableReusePortC fd

joinMulticast :: CInt -> CInt -> CInt -> IO CInt
joinMulticast sock destaddr ifaddr = do
  c_joinMulticast sock destaddr ifaddr

leaveMulticast :: CInt -> CInt -> CInt -> IO CInt
leaveMulticast sock destaddr ifaddr = do
  c_leaveMulticast sock destaddr ifaddr

setMulticastTTL sock ttl = do
  let fd = cast $ fdSocket sock
  c_setMulticastTTL fd $ cast ttl


foreign import ccall "testFunction" testFunctionC :: CInt -> IO ()
foreign import ccall "enableReusePort" enableReusePortC :: CInt -> IO CInt
foreign import ccall "joinMulticast" c_joinMulticast :: CInt -> CInt -> CInt -> IO CInt
foreign import ccall "setMulticastTTL" c_setMulticastTTL :: CInt -> CInt -> IO CInt
foreign import ccall "leaveMulticast" c_leaveMulticast :: CInt -> CInt -> CInt -> IO CInt

data PosAndSeq = PosAndSeq !Int !Int

data DetectedPIDS = DetectedPIDS {
    dpidAudio :: Maybe Int,         -- detected audio pid
    dpidAudioTime :: Word64,         -- last seen (granular, updated once a second)
    dpidVideo :: Maybe Int,         -- detected video pid
    dpidVideoTime :: Word64,          -- last seen (granular, updated once a second)
    dpidLastPAT :: Int
} deriving Show

data CircularBuffer = CircularBuffer
                            Int         -- ^ size in 188 blocks
                            (Ptr Word8)   -- buffer of (188*size) bytes
                            (IORef PosAndSeq)   -- next free position (in 188 blocks), next sequence
                            (IORef Double)       -- last addition time
                            (IORef DetectedPIDS)       -- last addition time

showStats s = do
    ds <- readIORef displayStatsVar
    when ds $ showLog $ "STATS: "++s

showDebug s = do
    ds <- readIORef displayDebugVar
    when ds $ showLog $ "DEBUG: "++s

ifDebug act = do
    ds <- readIORef displayDebugVar
    when ds $ act

showLog s = do
    ct <- getCurrentTime
    tz <- getCurrentTimeZone
    let lt = show $ utcToLocalTime tz ct
    putStrLn $ lt ++" relaying("++(show currentPID)++") "++s
    hFlush stdout

mkCircularBuffer :: Int -> IO CircularBuffer
mkCircularBuffer bytes =
    mkCircularBuffer0 bytes mallocArray

mkCircularBuffer0 :: Int -> (Int -> IO (Ptr Word8)) -> IO CircularBuffer
mkCircularBuffer0 bytes allocator = do
    -- let bytes = bufferSize config
    let nbuffers = (bytes `div` (188 * 7)) * 7
    showLog $ "INFO: Block count      : 188 bytes"
    showLog $ "INFO: Blocks allocated : " ++ (show nbuffers)
    showLog $ "INFO: Bytes allocated  : " ++ (show $ nbuffers * 188)
    -- buf <- mallocArray (nbuffers * 188) :: IO (Ptr Word8)
    buf <- allocator (nbuffers * 188) :: IO (Ptr Word8)
    lastAdd <- newIORef 0
    nextPosSeq <- newIORef (PosAndSeq 0 0)
    detected <- newIORef $ DetectedPIDS Nothing (-1) Nothing (-1) (-1)
    return $ CircularBuffer nbuffers buf nextPosSeq lastAdd detected


class Closable x where
    closeIt :: x -> IO ()

data MyClosable = forall a. Closable a => MyClosable a
data DummyClosable = DummyClosable


instance Closable Socket where
    closeIt s = sClose s

instance Closable Handle where
    closeIt h = hClose h

instance Closable DummyClosable where
    closeIt _ = return ()

instance Closable MyClosable where
    closeIt (MyClosable q) = closeIt q

receiver httpUrl@(_:_) [] (CircularBuffer size pkts nextFreePosSeq _ _) multicastRelay _ httpAsRaw userAgent compressorFilter = do
    let uri = fromJustX ("unable to parse this url: " ++ httpUrl) $ parseURI httpUrl
    let host = fromJustX ("http host name not specifieduried for receiver: "++ show (uri, httpUrl)) $ uriRegName uri
    haddr <- hostAddress <$> (getHostByName host)
    let port = cast $ fromMaybe 80 $ uriPort uri
    let portNo = PortNum $ swapBytes port
    closable <- newIORef $ MyClosable DummyClosable
    forever $ do
        showLog "INFO: http receiver start"
        rsocket <- socket AF_INET Stream defaultProtocol
        writeIORef closable $ MyClosable rsocket
        catchIOError ( do		-- backported code, don't blame
            catchIOError ( do
                connect rsocket (SockAddrInet portNo haddr)
                showLog "INFO: connected master (input) HTTP stream"
                let sendReq = "GET "++(uriPath uri)++" HTTP/1.1\r\n"
                             ++ "User-Agent: " ++ userAgent ++ "\r\n"
                             ++ "Host: "++host++"\r\n"
                             ++ "\r\n"
                putStrLn $ "sending:" ++ (show sendReq)
                send rsocket sendReq
                h <- socketToHandle rsocket ReadMode
                writeIORef closable $ MyClosable h
                let echoHGetLine h' = do
                    l <- hGetLine h'
                    return l
                if (not httpAsRaw) then do
                    whileM_ ( (/= "\r" ) <$> echoHGetLine h) (return ())
                  else
                    putStrLn "ignoring possible HTTP response"
                cfilter <- case compressorFilter of
                            "" -> createNullFilter h
                            _ -> createFilter compressorFilter h
                -- bracket (destroyFilter cfilter) $ do
                whileM_ (do
                            PosAndSeq nfp _ <- readIORef nextFreePosSeq
                            let toWrite = pkts `plusPtr` (nfp * 188)
                            rd <- receivePacket cfilter toWrite
                            -- rd <- hGetBuf h toWrite 188
                            return $ rd == 188
                        ) (do
                            modifyIORef nextFreePosSeq (\x@(PosAndSeq nfp ns) ->
                                            (
                                                let x' = PosAndSeq ((nfp + 1) `mod` size) (ns + 1) -- new value for ioref
                                                in x `seq` x'
                                            ))
                            case multicastRelay of
                                Just chan -> do
                                    PosAndSeq nfp _ <- readIORef nextFreePosSeq
                                    when (nfp `mod` 7 == 0) $
                                        writeChan chan $! (nfp - 7 + size) `mod` size  -- data ready 7 blocks
                                _ -> return ()
                            )
                hClose h
                threadDelay 10000000
              ) (\ex -> do
                showLog $ "WARN: master stream error: "++(show ex)
                readIORef closable >>= closeIt
                threadDelay 10000000
              )
          ) (\ex -> do
                showDebug $ "probably KillThread?: "++(show ex)
                readIORef closable >>= closeIt
                mtid <- myThreadId
                killThread mtid
            )
        return ()

receiver [] multicastAddr@(_:_) cb multicastRelay receiveMulticastInterfaces _ _ compressorFilter = do
    let multicastUrl = if "udp" == take 3 multicastAddr then multicastAddr else "udp://"++multicastAddr
    let uri = fromJustX ("unable to parse multicast: " ++ multicastUrl) $ parseURI multicastUrl
    haddr <- hostAddress <$> (getHostByName $
        fromJustX "multicast IP address not specifieduried for receiver" $ uriRegName uri)
    let port = cast $ fromMaybe 1234 $ uriPort uri
    let portNo = PortNum $ swapBytes port
    when ((null $ receiveMulticastInterfaces) || (null $ receiveMulticastInterfaces !! 0) ) $ error "please specify --receiveMulticastInterface"
    iaddrs <- mapM (\mci -> do
            let receiveMulticastInterfaceURL = if "udp" == (take 3 $ mci) then mci  else "udp://"++mci++"/"
            let iuri = fromJustX ("cannot parse receiveMulticastInterface: " ++ receiveMulticastInterfaceURL) $ parseURI receiveMulticastInterfaceURL
            hostAddress <$> (getHostByName $
                fromJustX "cannot parse receiveMulticastInterface" $ uriRegName iuri) ) receiveMulticastInterfaces
    forever $ do
        rsocket <- socket AF_INET Datagram defaultProtocol
        let fd = cast $ fdSocket rsocket
        catchIOError ( do
        enableReusePort rsocket
        setSocketOption rsocket ReuseAddr (-1)
        bindSocket rsocket (SockAddrInet portNo 0)
        showLog "Before join multicast"
        mapM_ (\iaddr -> joinMulticast fd (cast haddr) $ cast iaddr ) iaddrs
        showLog "After join multicast"
        -- h <- fdToHandle $ cast fd
        cfilter <- case compressorFilter of
                    "" -> createNullFilterUDP rsocket
                    _ -> createFilterUDP compressorFilter rsocket
        -- bracket (destroyFilter cfilter) $ do
        catchIOError ( do
                    let
                        fun :: IO ()
                        fun = do
                                    success <- receiveIntoBuffer7 cb (\(nfp,ptr) -> do
                                        (cnt,_) <- recvBufFrom rsocket ptr 1500
                                        -- cnt <- hGetBuf h ptr 1500
                                        case multicastRelay of
                                            Just channel -> writeChan channel nfp
                                            _ -> return ()
                                        return cnt)
                                    if success == 7 * 188 then fun else return ()
                    fun
                    showLog "Before leave multicast (1)"
                    mapM_ (\iaddr -> leaveMulticast fd (cast haddr) $ cast iaddr ) iaddrs
                    showLog "After leave multicast (1)"
                    sClose rsocket
                    -- hClose h
                  ) (\ex -> do
                    showLog $ "WARN: multicast master stream: "++(show ex)
                    mapM_ (\iaddr -> leaveMulticast fd (cast haddr) $ cast iaddr ) iaddrs
                    sClose rsocket
                    threadDelay 1000000
                  )
          ) (\ex -> do
                showDebug $ "probably KillThread?: "++(show ex)
                mapM_ (\iaddr -> leaveMulticast fd (cast haddr) $ cast iaddr ) iaddrs
                sClose rsocket
                mtid <- myThreadId
                killThread mtid
            )
        return ()


receiver _ _ _ _ _ _ _ _ = error "Must specify receiveHttpURL or receiveHttpURL"

receiveIntoBuffer7 (CircularBuffer size pkts nextFreePosSeq _ detected) recvr = do
    PosAndSeq nfp ns' <- readIORef nextFreePosSeq
    let toWrite = pkts `plusPtr` (nfp * 188)
    cnt <- recvr (nfp,toWrite)
    if (cnt == 7 * 188) then do
        det <- readIORef detected
        let ndet = foldl (maybeUpdateDet ns' toWrite) det $ map (\ix -> toWrite `plusPtr` (ix * 188)) [0..6]
        writeIORef detected $! ndet
        modifyIORef nextFreePosSeq (\x@(PosAndSeq nfp_ ns) ->
                        let x' = PosAndSeq ((nfp_ + 7) `mod` size) (7 + ns)
                        in x `seq` x'
                        )
        return cnt
      else
        return cnt
  where
    maybeUpdateDet baseIndex basePtr prevDet currentPtr =
        let itsIndex = baseIndex + (currentPtr `minusPtr` basePtr) `div` 188
        in if isPATPacket $ decodePacket currentPtr currentPtr itsIndex
            then {- trace ("detected PAT: "++show itsIndex) $ -} prevDet { dpidLastPAT = itsIndex }
            else prevDet



readHeaders h acc = do
    l <- hGetLine h
    if (l == "\r") then return acc else readHeaders h (l:acc)

parseHTTPRequest' :: (Stream s m Char) => ParsecT s u m (String,[(String,String)])
parseHTTPRequest' = do
    string "GET"
    spaces
    retval <- pathWithArgs
    spaces
    string "HTTP/1."
    return retval
  where
    pathWithArgs = do
        path <- many (noneOf "? ")
        stuff <- optionMaybe $ try $ do
            p1 <- (char '?' >> nv)
            p2 <- optionMaybe $ many (char '&'>> nv)
            return $ p1:(fromMaybe [] p2)
        return (path, fromMaybe [] stuff)
    nv = do
        name <- many1 (noneOf " =&")
        value <- optionMaybe $ try $ do
            char '='
            many (noneOf " &")
        return (name, fromMaybe "" value)

parseHTTPRequest :: String -> Maybe (String, [(String,String)])
parseHTTPRequest str =
    case parse parseHTTPRequest' "<request>" str of
        Left _ -> Nothing
        Right val -> Just val

parseHeaders = (catMaybes . map parseHeader )
parseHeader [] = Nothing
parseHeader s =
    let (a, b) = break (==':') s
    in if null b then Nothing else Just (a, tail b)

data AdvertisementAction = NoAdvertisement | DisplayFixedContent String | DisplayInternalError
    deriving (Read,Show,Eq)

streamMpegToSocket fh mpeg _ = do
    bracket
        (openBinaryFile mpeg ReadMode)
        (hClose)
        (\h -> do
            if (endsWith ".ts" mpeg) then do
                let prefillNext = 3000000       -- will be exiting this time earlier
                let fileDuration = 14000000 - prefillNext
                startTime <-  timeSpecToMicros <$> getTime Realtime
                s <- LBS.hGetContents h
                LBS.hPut fh s
                whileM (timeSpecToMicros <$> getTime Realtime >>= \nowTime -> return $ (fileDuration) - (nowTime - startTime) > 0) $ do
                    threadDelay 500000
                    LBS.hPut fh $ LBS.pack $ take (7*188) [255,255..]
                print ("exit delay")
              else do
                let pktsize = (188*7) + 4 + 4
                _ <- fromEnum <$> hFileSize h
                allocaArray pktsize $ \ptr -> do
                    startStreamingTime_  <-  timeSpecToMicros <$> getTime Realtime
                    let startStreamingTime = startStreamingTime_ - 500000
                    hGetBuf h ptr pktsize
                    firstPacketTime <- timeSpecToMicros <$> getTimeFromPtr ptr
                    let realTimeAdvance = startStreamingTime - firstPacketTime
                    hSeek h AbsoluteSeek 0
                    let loop = do
                        rd <- hGetBuf h ptr pktsize
                        if (rd == pktsize) then do
                            thisPacketTime <- timeSpecToMicros <$> getTimeFromPtr ptr
                            let thisPacketStreamTime = thisPacketTime + realTimeAdvance
                            nowTime <- timeSpecToMicros <$> getTime Realtime
                            let timeToSleep = thisPacketStreamTime - nowTime
                            when (timeToSleep > 0) $ do
                                print ("time to sleep",timeToSleep)
                                threadDelay (fromEnum timeToSleep)
                            hPutBuf fh (ptr `plusPtr` 8) (188*7)
                            loop
                         else
                            return ()
                    loop
                return ()
                )
    where
        getTimeFromPtr :: Ptr Word8 -> IO TimeSpec
        getTimeFromPtr ptr = do
            let iptr = castPtr ptr :: Ptr Word32
            a <- peek iptr
            b <- peek (iptr `plusPtr` 1 :: Ptr Word32)
            -- print ("peek",a,b)
            return $ TimeSpec (fromIntegral a) (fromIntegral b)
        timeSpecToMicros (TimeSpec sec_ nsec_) =
            (toEnum sec_ :: Int64) * 1000000 + (toEnum nsec_ :: Int64) `div` 1000



streamBuffer :: CircularBuffer -> Handle -> Int -> Int64 -> IO ()
streamBuffer (CircularBuffer size pkts nextFreePosSeq _ _) h prefillBytes maxPackets = do
    PosAndSeq _ toSendSeq <- readIORef nextFreePosSeq
    -- send something to client on connect
    let prefillSegments = prefillBytes `div` 188
    let beginStreamingSeq = findPAT (max (toSendSeq - prefillSegments) 0) toSendSeq
    ifDebug $ print ("waitSince: ",beginStreamingSeq,"was:",toSendSeq - prefillSegments)
    waitSince beginStreamingSeq maxPackets
  where
        waitSince :: Int -> Int64 -> IO ()
        waitSince !sseq mp
            | mp <= 0 = return ()
            | otherwise = do       -- main loop
                PosAndSeq nfp ns <- readIORef nextFreePosSeq
                if (ns > sseq) then do
                        -- send from [sseq to ns)
                        print ("sending...", ns, sseq)
                        let !tailLength = (ns - sseq) `mod` size
                        ifDebug $ print ("high mark:",ns, "low mark:",sseq,"tailLength:",tailLength)
                        allocaArray (188*tailLength) $ \(arr :: Ptr Word8) -> do
                            mapM_ (\(!ix) -> do
                                    let blockNo = (nfp - ix + size - 1) `mod` size
                                    let ptr = (pkts `plusPtr` (blockNo * 188))
                                    copyArray ((arr `plusPtr` ((tailLength-1-ix)*188)) :: Ptr Word8) (ptr :: Ptr Word8) 188
                                    -- hPutBuf h ptr 188
                                       ) [(tailLength - 1),(tailLength - 2).. 0]    -- offsets from head of the queue
                            ifDebug $ print ("sent ",(188*tailLength), "bytes")
                            hPutBuf h arr (188*tailLength)
                         -- hFlush h
                        -- print ("looping with",(mp-(toEnum tailLength)))
                        waitSince ns (mp-(toEnum tailLength))
                    else do
                        ifDebug $ print ("delay (no data in buffer to send to client: receiver stopped working?)...", ns, sseq)
                        threadDelay 50000
                        waitSince sseq mp
        findPAT fromSeq toSeq
            | fromSeq == toSeq = toSeq
            | otherwise =
                let ptr = pkts `plusPtr`  (188 * (fromSeq `mod` size))
                in if isPATPacket $ decodePacket ptr ptr 0
                    then fromSeq
                    else findPAT (fromSeq+1) toSeq


maxint = 0x7FFFFFFF :: Int64

httpBroadcaster :: String -> CircularBuffer -> IORef Int -> Int -> ((String,String,String,Bool) -> IO AdvertisementAction) -> IO ()
httpBroadcaster [] _ _ _ _ = return ()
httpBroadcaster bcAddr cb clientCount prefillBytes advertCallback = do
    showLog $ "INFO: le http broadcaster start (s tochkami)"
    showLog $ "INFO: http broadcaster prefill bytes = "++(show prefillBytes)
    let broadCastAddr = if "http" == take 4 bcAddr then bcAddr else "http://"++bcAddr
    let uri = fromJustX ("unable to parse broadcastHttpAddr address: " ++ broadCastAddr) $ parseURI broadCastAddr
    haddr <- hostAddress <$> (getHostByName $
        fromJustX "broadcastHttpAddr not specifieduried for http broadcast" $ uriRegName uri)
    let port = cast $ fromMaybe 80 $ uriPort uri
    let portNo = PortNum $ swapBytes port
    lsocket <- socket AF_INET Stream defaultProtocol
    enableReusePort lsocket
    setSocketOption lsocket ReuseAddr (-1)
    bindSocket lsocket $ (SockAddrInet portNo haddr)
    listen lsocket 20
    forever $ catchIOError (do
        (ssock,addr) <- accept lsocket
--         setSocketOption ssock NoDelay (-1)
        h <- socketToHandle ssock ReadWriteMode
        forkIO $ do
            catchIOError (do
                atomicModifyIORef clientCount (\x -> (1+x, 0))
                query <- hGetLine h
                let (_,parsedArgs) = (fromMaybe ("/",[]) $ parseHTTPRequest query)
                print ("parsedArgs",parsedArgs)
                hdrs <- parseHeaders <$> readHeaders h []
                pid <- getProcessID
                sockName <- getSocketName ssock
                let foreignAddr = (fst . break (==':') . show) addr
                putStrLn $ "HTTP_CLIENT\t" ++ (show pid) ++ "\t" ++ (show sockName) ++ "\t" ++ (foreignAddr)++"\t" ++ (show $ lookup "User-Agent" hdrs)
                showDebug $ "client headers: "++(show hdrs)
                hFlush stdout
                hPutStr h "HTTP/1.0 200 OK\r\n"
                hPutStr h "Content-type: video/mpeg\r\n"
                hPutStr h "Cache-Control: no-cache\r\n\r\n"
                hSetBuffering  h (NoBuffering)
                cc <- readIORef clientCount
                showDebug $ "httpBroadcaster client count increased to:" ++ (show cc)
                advertAction <- advertCallback (foreignAddr, fromMaybe "" $ lookup "sig" parsedArgs, fromMaybe "" $ lookup "User-Agent" hdrs, True)
                let regularStreaming = do
                    showLog ("calling streamBuffer")
                    streamBuffer cb h prefillBytes maxint
                    showLog ("exit streamBuffer")
                    showLog "ERROR: should not happen"
                catchIOError (
                  do
                    case advertAction of
                        DisplayInternalError -> return ()
                        DisplayFixedContent filename -> do
                            streamMpegToSocket h filename prefillBytes
                            regularStreaming
                        _ -> do
                            regularStreaming
                 ) (\e -> do
                        advertCallback (foreignAddr, fromMaybe "" $ lookup "sig" parsedArgs, fromMaybe "" $ lookup "User-Agent" hdrs, False)
                        throw e
                        )
              ) (\ex -> do
                showLog $ "WARN: http broadcaser client exception: "++(show ex)
                )
            atomicModifyIORef clientCount (\x -> (x-1, ()))
            hClose h
        return ()
      ) (\e -> do
        showLog $ "ERR: accept failed, retrying: " ++ (show e)
        sClose lsocket
        threadDelay 1000000
        httpBroadcaster  bcAddr cb clientCount prefillBytes advertCallback
      )
    return ()

multicastBroadcaster [] _ _ _ _ = return ()
multicastBroadcaster _ Nothing _ _ _ = return ()
multicastBroadcaster bcAddr (Just chan) (CircularBuffer _ pkts _ _ _) clientCount broadcastMulticastTTL = do
    let multicastUrl = if "udp" == take 3 bcAddr then bcAddr else "udp://"++bcAddr
    let uri = fromJustX ("unable to parse multicast sender address: " ++ multicastUrl) $ parseURI multicastUrl
    haddr <- hostAddress <$> (getHostByName $
        fromJustX "multicast IP address not specifieduried for receiver" $ uriRegName uri)
    let port = cast $ fromMaybe 4567 $ uriPort uri
    let portNo = PortNum $ swapBytes port
    let ttl = broadcastMulticastTTL
    ssocket <- socket AF_INET Datagram defaultProtocol
    when (ttl < 1 || ttl > 255) $
        fail "multicast ttl must be in range [1..255]"
    enableReusePort ssocket
    setSocketOption ssocket ReuseAddr (-1)
    setMulticastTTL ssocket ttl
    showLog "INFO: multicast broadcaster launched!"
    writeIORef clientCount 1
    let
        fun :: IO ()
        fun = do
        nfp <- readChan chan
        -- print ("sending mcast packets offset", nfp)
        sendBufTo ssocket (pkts `plusPtr` (nfp * 188)) (7 * 188) (SockAddrInet portNo haddr)
        fun
    fun
    return ()

readIORefDebug nm var = do
        v <- readIORef var
        showDebug $ nm ++ "=="++(show v)
        return v

startStop clientCount rcvr = do
    forever $ do
        whileM_ ( ( < 1) <$> readIORefDebug "clientCount" clientCount) (threadDelay 100000)
        cnt <- readIORef clientCount
        showDebug $ "starting receiver: "++(show cnt)
        tid <- forkIO rcvr
        whileM_ ( ( > 0) <$> readIORefDebug "clentCount" clientCount) (threadDelay 100000)
        showDebug "killing receiver"
        killThread tid

usr1handler = Catch $ do
        showLog $ "INFO: USR1 signal caught"
        modifyIORef displayStatsVar not

usr2handler = Catch $ do
        showLog $ "INFO: USR2 signal caught"
        toggleDebug

toggleDebug = do
        modifyIORef displayDebugVar not

testCase = do
    thr <- forkIO $ do
        catchIOError (do
            lsocket <- socket AF_INET Stream defaultProtocol
            let portNo = PortNum $ swapBytes 6666
            bindSocket lsocket $ (SockAddrInet portNo 0)
            listen lsocket 20
            print "before"
            (_,_) <- accept lsocket
            print "after"
            return ()
          ) (\_ -> do
            print "ohoho!" )
    threadDelay 1000000
    killThread thr
    threadDelay 1000000

