-----------------------------------------------------------------------------
--
-- Module      :  RelayingServer.Filter
-- Copyright   :  San
-- License     :  AllRightsReserved
--
-- Maintainer  :  San
-- Stability   :  provisional
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module RelayingServer.Filter where

import System.Process
import System.IO
import Data.Word
import Data.IORef
import Data.Either()
import Data.Maybe
import Foreign.Ptr
import Foreign.Marshal
import Control.Concurrent
import Control.Monad
import Network.Socket

data FilterData = FilterData {
        fdExternalDataHandle :: Either Handle Socket,
        fdProcess :: String,
        fdProcessHandle :: IORef (Maybe Handle, Maybe Handle, Maybe Handle, ProcessHandle),
        fdCopierThread :: Maybe ThreadId
        }

withMaybe (Just a) f = f a
withMaybe (Nothing) _ = return ()

createNullFilter :: Handle -> IO FilterData
createNullFilter h = do
    return $ FilterData (Left h) "" undefined Nothing

createNullFilterUDP :: Socket -> IO FilterData
createNullFilterUDP sock = do
    return $ FilterData (Right sock) "" undefined Nothing

createFilter :: String -> Handle -> IO FilterData
createFilter procname h = do
    let cp = CreateProcess (ShellCommand procname) Nothing Nothing (UseHandle h) CreatePipe Inherit True False
    ph <- createProcess cp
    fph <- newIORef ph
    return $ FilterData (Left h) procname fph Nothing

createFilterUDP :: String -> Socket -> IO FilterData
createFilterUDP procname sock = do
    let cp = CreateProcess (ShellCommand procname) Nothing Nothing CreatePipe CreatePipe Inherit True False
    ph@(hin,_,_,_) <- createProcess cp
    fph <- newIORef ph
    ptr <- mallocArray 1500 :: IO (Ptr Word8)
    tid <- forkIO $ forever $ do
        (cnt,_) <- recvBufFrom sock ptr 1500
        if (cnt == 7 * 188) then hPutBuf (fromJust hin) ptr $ 7 * 188
            else return ()
    return $ FilterData (Right sock) procname fph (Just tid)

receivePacket :: FilterData -> Ptr Word8 -> IO Int
receivePacket fd ptr = do
    case (fdProcess fd, fdExternalDataHandle fd) of
        ("", Left h) -> hGetBuf h ptr 188
        ("", Right sock) -> do
            (cnt,_) <- recvBufFrom sock ptr 1500
            return cnt
        (_, Left _) -> do
            (_, hOut, _, _) <- readIORef (fdProcessHandle fd)
            case hOut of
                Just hOut_ -> hGetBuf hOut_ ptr 188
                Nothing -> do
                    putStrLn "invalid out handle of filter process"
                    threadDelay 100000
                    return 0
        (_, Right _) -> do
            (_, hOut, _, _) <- readIORef (fdProcessHandle fd)
            case hOut of
                Just hOut_ -> hGetBuf hOut_ ptr (7*188)
                Nothing -> do
                    putStrLn "invalid out handle of filter process"
                    threadDelay 100000
                    return 0

destroyFilter :: FilterData -> IO ()
destroyFilter fd = do
    when (fdProcess fd /= "") $ do
        withMaybe (fdCopierThread fd) killThread
        (_,_,_,ph) <- readIORef (fdProcessHandle fd)
        terminateProcess ph
        return ()

