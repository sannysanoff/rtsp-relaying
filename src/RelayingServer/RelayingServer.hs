{-# OPTIONS -XForeignFunctionInterface -XBangPatterns -XDeriveDataTypeable #-}
-- vim: sw=4:ts=4:expandtab
-----------------------------------------------------------------------------
--
-- Module      :  Main.Server
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------



module Main where

import Control.Concurrent
import Control.Monad
import Control.Applicative
import Data.IORef
import Data.Maybe
-- import System.Posix.Types
import System.Console.CmdArgs
import System.IO.Unsafe
import System.Posix.Signals
import System.Process
import System.Posix.Files
-- import Utils.SanUtils
import RelayingServer.Relaying
import Utils.SanUtils

data Config = Config { receiveHttpURL :: String,
                        receiveMulticast :: String,
                        receiveMulticastInterface :: String,
                        httpUserAgent :: String,
                        broadcastHttpAddr :: String,
                        broadcastMulticastAddr :: String,
                        broadcastMulticastTTL :: Int,
                        displayStats :: Bool,
                        displayDebug :: Bool,
                        httpAsRaw :: Bool,
                        bufferSize :: Int,
                        prefillBytes :: Int,
                        prejoinMulticastGroup :: Bool,
                        mpegTSCompressorFilter :: String,
                        authServer :: String,
                        authScript :: String
                         } deriving (Show, Data, Typeable)

{-- Dies ist eine globale Variable!!!!! -}

is = groupname "Input Stream (select only one)"
os = groupname "Output Stream (choose one or two)"
memorry = groupname "Memory"

config  = unsafePerformIO $ cmdArgsRun $ cmdArgsMode $ Config {
                                                receiveHttpURL = "" &= is &= help "[none] HTTP URL to connect and take stream from (e.g http://something:12345/stream/name)" &= name "inhttp" &= explicit,
                                                httpUserAgent = defaultUserAgent &= is &= help "[none] HTTP user agent to use for downloading ("++defaultUserAgent++")" &= name "useragent" &= explicit,
                                                receiveMulticast = "" &= is &= help "[none] Multicast stream to join (e.g. 224.0.0.22:12345)" &= name "inudp"  &= explicit,
                                                receiveMulticastInterface = "" &= is &= help "[none] multicast interface name (IP) on local host" &= name "inudpif"  &= explicit,
                                                broadcastHttpAddr = "" &= os &= help "[none] Local socket address (interface:port) to serve HTTP" &= name "outhttp"  &= explicit,
                                                broadcastMulticastAddr = "" &= os &= help "[none] Multicast address (interface:port) to serve multicast UDP" &= name "outudp" &= explicit,
                                                broadcastMulticastTTL = 1 &= os &= help "[1] Multicast TTL" &= name "outttl" &= explicit,
                                                displayStats = False &= help "[off] Display Stats" &= name "stats" &= explicit,
                                                displayDebug = False &= help "[off] Display Low-level debug" &= name "debug" &= explicit,
                                                httpAsRaw = False &= help "[off] Don't expect HTTP response from source" &= name "rawhttp" &= explicit,
                                                bufferSize = 10000000 &= memorry &= help "[10000000] Circular buffer size to store stream (in bytes)" &= name "bs"  &= explicit,
                                                prejoinMulticastGroup = False &= is &= help "[False] always join multicast" &= name "pj"  &= explicit,
                                                prefillBytes = -1 &= memorry &= help "[bufferSize/10] part of buffer (bytes) to send to client immediately " &= name "pf"  &= explicit,
                                                mpegTSCompressorFilter = "" &= help "[none] MPEG-TS recompression process" &= name "compressor"  &= explicit,
                                                authServer = "" &= help "[none] evil auth server " &= name "auth"  &= explicit,
                                                authScript = "/etc/relaying.auth.sh" &= help "[/etc/relaying.auth.sh] evil auth shell script (permits everything if missing) " &= name "auths"  &= explicit
                                                } &= summary  "IPTV http/multicast relay, version 0.2e-235\nPowered by Haskell."
                                                  &= program "relaying"

advertCallback :: (String,String,String,Bool) -> IO AdvertisementAction
advertCallback (ip,token,agent,joined) = do
    doScript <- fileExist $ authScript config
    putStrLn $ "callback "++show (ip,token,agent,joined)
    if (doScript) then do
            t1 <- currentTimeMillis
            retval <- readProcess "/bin/sh" [authScript config, ip,token,agent,if joined then "1" else "0"] ""
            t2 <- currentTimeMillis
            showLog $ "Script auth time="++show (t2-t1)++" msec, retval="++retval
            case fmap fst . listToMaybe . reads $ retval of
                Nothing -> do
                    showLog $ "ERROR: Bad parse of AdvertismentAction: "++show retval
                    return DisplayInternalError
                Just a -> return a
        else do
            --return $ DisplayFixedContent "videos/out/anke-short.ts"
            return $ NoAdvertisement


runServer = do
    installHandler sigUSR1 usr1handler Nothing
    installHandler sigUSR2 usr2handler Nothing
    when (displayDebug config) $ toggleDebug
    multicastRelay <- if broadcastMulticastAddr config /= [] then Just <$> newChan  else return Nothing        -- offsets in buffer to send
    buffer@(CircularBuffer _ _ nextSeqPos _ _) <- mkCircularBuffer $ bufferSize config
    clientCount <- newIORef $ if prejoinMulticastGroup config then 1 else 0
    let prefill = case prefillBytes config of
                    -1 -> bufferSize config `div` 10
                    pf -> min (bufferSize config `div` 2 ) pf
    doScriptAuth <- fileExist (authScript config)
    when doScriptAuth $ showLog $ "Will do script auth using script "++authScript config
    forkIO $ httpBroadcaster (broadcastHttpAddr config) buffer clientCount prefill advertCallback
    forkIO $ multicastBroadcaster (broadcastMulticastAddr config) multicastRelay buffer clientCount (broadcastMulticastTTL config)
    forkIO $ forever $ do
                PosAndSeq pos_ nextSeq_  <- readIORef nextSeqPos
                showStats $ show (pos_, nextSeq_)
                threadDelay 1000000
    startStop clientCount $
        receiver
            (receiveHttpURL config)
            (receiveMulticast config)
            buffer
            multicastRelay
            [(receiveMulticastInterface config)]
            (httpAsRaw config)
            (httpUserAgent config)
            (mpegTSCompressorFilter config)
    return ()

main = do
    writeIORef displayStatsVar $ displayStats config
    runServer
--main = testCase


