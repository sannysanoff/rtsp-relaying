module Data.Queue.SimplestQueue where

data Que a = Que [a] [a]

que_isempty (Que [] []) = True
que_isempty _ = False
que_dequeue (Que [] []) = error "que_dequeue: empty equeue"
que_dequeue (Que (x:xs) ys) = (x, Que xs ys)
que_dequeue (Que [] ys) = que_dequeue $ Que (reverse ys) []
que_enqueue (Que xs ys) y = Que xs (y:ys)
que_dequeueall (Que xs ys) = ys ++ (reverse xs)  -- head of list returned is last element
que_enqueueall (Que xs ys) yy = Que xs (yy++ys)     -- head of list is last element
que_empty = Que [] []
que_length (Que xs ys) = (length xs) + (length ys)

