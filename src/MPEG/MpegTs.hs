-- {-# OPTIONS -fglasgow-exts #-}
module MPEG.MpegTs where
-- vim: sw=4:ts=4:expandtab

import System.IO
import System.IO.Error
--import System.Time
import Data.Maybe
import Data.List
import Data.Word
import Data.IORef
import Data.ByteString.Internal
import qualified Data.ByteString.Lazy as BS
-- import qualified Data.ByteString as S
import Control.Monad
import Control.Concurrent
import Network.Socket
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.Ptr
import Text.URI
import System.Clock

import MPEG.MpegTypes
import Utils.SanUtils
import Codec.MpegTS.Formats
import MPEG.Server
import Net.RTSP.RtspTypes
import Net.RTSP.RtspFormats
import Data.Queue.SimplestQueue
import CapturingServer.Storage


serveRTSP' h ho sa@(SockAddrInet _ clientIp) srvr cleanup replayDirectory =
  do
        -- print "expecting..."
        expect "serverRTSP-isValidRequest" h isValidRequest (\cmd -> do
            let
                readHeaders :: [(String,String)] -> IO [(String,String)]
                readHeaders acc = do
                    hdr <- expect "serveRTSP-isEmptyLine" h (\s -> isEmptyLine s || (isRight $ decodeHeader s)) return
                    if (hdr == Nothing) then do
                            putStrLn "sending: hdr parse fail"
                            hPutStrLn ho "header parse fail"
                            return []
                       else if isEmptyLine (fromJustX "hdr@sRTSP'" hdr) then return acc else readHeaders $ (fromRight $ decodeHeader $ fromJustX "(2)hdr@sRTSP'" hdr):acc
                cmd' = decodeRTSPRequest cmd
            -- print $ "got cmd!! : "++(show cmd)
            headers <- readHeaders []
            let cseq = fromMaybeNVL (lookup "CSeq" headers) "0"
                transport = case lookup "Transport" headers of
                    Just val -> decodeTransport $ BS.pack $ map c2w val
                    Nothing -> []
                range = fromMaybeNVL (lookup "Range" headers) ""
                sessionNo = read (fromMaybeNVL (lookup "Session" headers) "-1") :: Int
                handleResponse (RTSPInvalidRequest _) =
                        hPutStr ho $ "RTSP/1.0 400 Invalid request\r\n\r\n"
                handleResponse (RTSPOptions _) =
                        hPutStr ho $ "RTSP/1.0 200 OK\r\nCSeq: "++cseq++"\r\nPublic: OPTIONS, DESCRIBE, SETUP, "++
                                        "TEARDOWN, PLAY, PAUSE, GET_PARAMETER, SET_PARAMETER\r\n\r\n"
                handleResponse (RTSPDescribe url) = do
                    rtspDescribe url ho cseq replayDirectory
                handleResponse (RTSPDummy _) = do
                    hPutStr ho $ "RTSP/1.0 400 Invalid request\r\n\r\n"
                handleResponse (RTSPSetup url) = do
                    print "setup 1"
                    clientIpStr <- inet_ntoa clientIp
                    print $ "setup 2: transport="++(show transport)
                    let [clientPort] = [x | (TWClientPort x _) <- transport]
                    session <- mkRTSPSession srvr clientIp clientPort url replayDirectory
                    modifyMVar cleanup (\l -> return ((releaseSession session srvr):l,()))
                    print "setup 3"
                    let (SockAddrInet (PortNum pn) ha) = rsServerSockAddr session
                    print "setup 4"
                    myIp <- inet_ntoa $ ha
                    print "setup 5"
                    let transport' = intersperse ";" $ map show $ (filter neededForPassBack transport) ++
                                   [(TWServerPort $ swapBytes $ fromEnum pn),TWSource myIp, TWDestination clientIpStr] :: [String]
                        neededForPassBack TWModePlay = False
                        neededForPassBack _ = True
                        response = concat $ intersperse "\r\n" $ [
                                    "RTSP/1.0 200 OK",
                                    "CSeq: "++cseq,
                                    "Transport: "++(concat $ transport'),
                                    "Session: "++(show $ rsNumber session),
                                    "",
                                    ""]
                    hPutStr ho response
                    print $ "setup last: response=\n"++response

                handleResponse (RTSPPause _) = do
                    maybeSession <- getSession srvr sessionNo
                    rtspPause maybeSession ho cseq pauseStreaming
                handleResponse (HTMLGet url) = do
                    let uri = fromJustX "htmlget1" $ parseURI url
                    let qi item = fromJust $ lookup item $ uriQueryItems uri
                    when (not $ containsText "favicon.ico" $ uriPath uri) $
                        putStrLn $ "HTTP GET "++(show $ uriPath uri)
                    case uriPath uri of
                        -- get time delay for streaming session
                        "/getTimeShiftingDiff" -> do
                            maybeSession <- getSessionByUserKey srvr $ uriQuery uri
                            print $ "session="++(show maybeSession)
                            htmlGetTimeShiftingDiff maybeSession ho
                        -- get status of specified storage (alive? segments)
                        "/getStatus" -> htmlGetStatus ho (fromJustX "html:getStatus" $ uriQuery uri) replayDirectory
                        -- get files of specified (start,length) program
                        "/getProgramFiles" -> do
                                htmlGetProgramFiles ho (read $ qi "ts" :: Int) (read $ qi "length" :: Int) (qi "storage") replayDirectory
                        -- link file to subdirectory (to prevent removal)
                        "/pinFile" -> htmlPinFile ho (uriQueryItems uri) replayDirectory
                        -- unlink file from subdirectory
                        "/unpinFile" -> htmlUnpinFile ho (uriQueryItems uri) replayDirectory
                        -- all storage directories timestamp (broadcast channels)
                        "/getAllBroadcastStatus" -> htmlGetAllBroadcastStatus ho replayDirectory
                        "/describeStream" -> htmlDescribeStream ho (uriQueryItems uri)
                        _ -> return ()
                    hClose ho
                handleResponse (RTSPPlay _) = do
                    maybeSession <- getSession srvr sessionNo
                    rtspStartPlay maybeSession ho cseq range startStreaming continueStreaming
--                handleResponse (RTSPPause _) = do
--                    maybeSession <- getSession srvr sessionNo
--                    rtspPause maybeSession ho cseq range startStreaming
                handleResponse (RTSPGetParameter _) = do
                    print "handle RTSPGetParameter"
                    maybeSession <- getSession srvr sessionNo
                    rtspGetParameter maybeSession ho cseq
                handleResponse (RTSPTeardown _) = do
                    maybeSession <- getSession srvr sessionNo
                    case (maybeSession) of
                        Just session -> do
                            releaseSession session srvr
                            hPutStr ho $ concat $ intersperse "\r\n" $ [
                                            "RTSP/1.0 200 OK",
                                            "CSeq: "++cseq,
                                            "",
                                            ""
                                            ]
                            hFlush ho
                        Nothing -> do
                            hPutStr ho $ concat $ intersperse "\r\n" $ [
                                            "RTSP/1.0 404 NOT FOUND!",
                                            "CSeq: "++cseq,
                                            "Session: "++(show sessionNo),
                                            ""]
            handleResponse cmd'
            closed <- hIsClosed ho
            when (not closed) $ do
                hFlush ho
                serveRTSP' h ho sa srvr cleanup replayDirectory
            )
        return ()

serveRTSP' _ _ _ _ _ _ = error "unmatched socket address"


-- serveTick Playing =

toHandle (HandleStreamSource fhandle) = fhandle
toHandle _ = error "toHandle: invalid argument"

startStreaming SourceFileTS sess = do
    streaming <- readMVar (rsStreaming sess)
    firstPes <- fmap (fromJustX "sstream") $ findFirstPES $ toHandle $ rssFileHandle streaming
    print $ "first pesPTS = "++(show $ pesPTS firstPes)
    ctm <- currentTimeMillisF
    let firstTS = (fromJustX "sstream2" $ pesPTS firstPes)
    let baseTm = ctm - firstTS
    print ("startStreaming: ctm",ctm,"baseTm",baseTm,"firstTS",firstTS)
    writeIORef (rsPlayState sess) (PSPlaying baseTm)
    forkIO $ streamer sess
    return ()

startStreaming SourceFileITS sess = do
    streaming <- readMVar (rsStreaming sess)
    blk@(ITSBlock tspec _) <- peekITSBlock $ rssFileHandle streaming
    print "startStreaming! destroyITSBlock"
    destroyITSBlock blk
    ctm <- currentTimeMillisF
    let baseTm = ctm - timeSpecToDouble tspec
    -- sample output: startStreaming SourceFileITS: basetime=
    --         (44794.759138822556,1.280604395504095e9,1.2805596007449563e9,TimeSpec {sec = 1280559600, nsec = 744956303})"
    print $ "startStreaming SourceFileITS: basetime="++(show (baseTm, ctm, timeSpecToDouble tspec,tspec))
    writeIORef (rsPlayState sess) (PSPlaying baseTm)
    forkIO $ streamer sess
    return ()

timeSpecToDouble (TimeSpec a b) =
    fromIntegral a + fromIntegral b / 1e9

destroyITSBlock (ITSBlock _ ptr) = do
    free (ptr `plusPtr` (-8))
    return ()

ensureReadPossible (StorageStreamSource _ segv) = do
    seg <- readIORef segv
    seg' <- maybeSwitchSegmentWhileReading seg
    writeIORef segv seg'
ensureReadPossible _ = error "ensureReadPossible: arg err"

isReadPossible (StorageStreamSource _ segv) = do
    seg <- readIORef segv
    canReadMore seg
isReadPossible _ = error "isReadPossible: arg err"


readITSBlock ss@(StorageStreamSource _ segv) = do
    ensureReadPossible ss
    (ITSSegment { segHandle = h }) <- readIORef segv
    let blkLength = 4+4+(188 * 7)
    buf <- mallocBytes blkLength
    -- pos <- hTell h
    hGetBuf h buf blkLength
    a <- peek buf :: IO Word32
    b <- peek (castPtr $ buf `plusPtr` 4) :: IO Word32
    let tspec = TimeSpec (fromEnum a ) (fromEnum b)
    let retval = ITSBlock tspec (buf `plusPtr` (4+4))
--    print ("readITSBlock tspec=",tspec,"pos=",pos)
    return retval
readITSBlock _ = error "readITSBlock: bad arg"

peekITSBlock ss@(StorageStreamSource _ segv) = do
    ensureReadPossible ss
    (ITSSegment { segHandle = handle }) <- readIORef segv
    pos <- hTell handle
    blk <- readITSBlock ss
    hSeek handle AbsoluteSeek pos
    return blk

peekITSBlock _ = error "peekITSBlock: bad arg"

continueStreaming SourceFileTS sess = do
    putStrLn "continue streaming!"
    ss <- takeMVar (rsStreaming sess)
    baseTm <- currentTimeMillisF >>= (\t -> return $ t - (rssStreamedPTS ss))
    writeIORef (rsPlayState sess) (PSPlaying baseTm)
    putMVar (rsStreaming sess) ss
    return ()

continueStreaming SourceFileITS sess = do
    putStrLn "continue streaming!"
    ss <- takeMVar (rsStreaming sess)
    modifyMVar_ (psPackets $ rssPacketSender ss) $ \packets -> do
        baseTime' <- currentTimeMillisF
        let packets' = rebasePackets $ que_dequeueall $ opsPackets packets
        return $ packets { opsPaused = False, opsBaseTime = baseTime', opsPackets = que_enqueueall que_empty packets', opsLastAddedPacket = safeHead packets' }

    let source = rssFileHandle ss
    ensureReadPossible source
    blk@(ITSBlock tspec _) <- peekITSBlock source
    destroyITSBlock blk
    ctm <- currentTimeMillisF
    let baseTm = ctm - timeSpecToDouble tspec

    writeIORef (rsPlayState sess) (PSPlaying baseTm)
    putMVar (rsStreaming sess) ss
    return ()
    where
        rebasePackets [] = []
        rebasePackets pkts =
            let (OutPacket { opTimeInBase = minTime }) = last pkts
            in map (\op -> op { opTimeInBase = opTimeInBase op - minTime }) pkts

pauseStreaming SourceFileTS sess = do
    writeIORef (rsPlayState sess) PSPaused

pauseStreaming SourceFileITS sess = do
    writeIORef (rsPlayState sess) PSPaused
    streaming <- readMVar $ rsStreaming sess
    modifyMVar_ (psPackets $ rssPacketSender streaming) $ \packets -> do
        return $ packets { opsPaused = True }

-- returns the range between first and last packet in the queue
outPacketsInterval :: OutPackets -> Maybe Double
outPacketsInterval ops =
    let que = opsPackets ops
    in if (que_isempty que) then Nothing
         else case (opsLastAddedPacket ops, Just $ fst $ que_dequeue que) of
                  (Just lst, Just frst) -> Just (opTimeInBase lst - opTimeInBase frst)
                  _ -> Nothing

playTimeFunction _ (PSPaused) _ ss = do
    threadDelay $ 100 * 1000
    return ss


playTimeFunction SourceFileTS (PSPlaying baseTime) _ ss = do
    ops <- readMVar $ psPackets $ rssPacketSender ss
    let fillQueueOnce = do
        -- putStrLn $ "PTS: fillQueueOnce"
        ctm <- currentTimeMillisF
        let lastPacketTime = case opsLastAddedPacket ops of
                Just pkt -> opTimeInBase pkt
                Nothing -> ctm - baseTime
--        streamUntil = lastPacketTime + 1.0 -- 1 second ahead
        ps_@(pkt:_) <- readPacketsUntilTS [] $ toHandle $ rssFileHandle ss
        -- putStrLn $ "PTS: fillQueueOnce: ps len="++show (length ps_)
        remains <- readPackets ((7 - (length ps_ `mod` 7)) `mod` 7) [] $ toHandle $ rssFileHandle ss
        let thisPacketTime = fromJustX "thisPacketTime" $ getPacketTS pkt
            ps = map (\pg -> OutPacket (map (\p -> (tspDataPtr p, free $ tspDataPtr p)) pg) 0) $ groupPackets (reverse ps_ ++ remains) []
            outPackets = alignPackets ps lastPacketTime thisPacketTime
        modifyMVar_ (psPackets $ rssPacketSender ss) (\ops_ -> do
            when (False) $ print ("here1","queue size:", "-1",
                        "queue interval:",outPacketsInterval ops,
                        "thisPacketTime:",thisPacketTime,
                        "lastPacketTime:",lastPacketTime)
            -- mapM_ (print . showOutPacket) outPackets
            let q' = que_enqueueall (opsPackets ops_) (reverse outPackets)
            -- putStrLn $ "TOP="++(showOutPacket $ fromJust $ QC.top q')
            return $ ops_ {
                opsBaseTime = baseTime,
                opsPackets = q',
                opsLastAddedPacket = Just $ last outPackets })
        return $ ss {
            rssStreamedPos = toInteger $ tspFileOffset pkt
            }
    case outPacketsInterval ops of
        Nothing -> fillQueueOnce
        Just range | range < 1 -> fillQueueOnce
        _ -> do
            threadDelay $ 100 * 1000
            return ss
    where
        alignPackets ps lastPacketTime thisPacketTime =
            let timeDiff = thisPacketTime - lastPacketTime
                timeStep = timeDiff / fromIntegral (length ps)
                ops = map (\((OutPacket pkts _),tm) -> OutPacket pkts tm) $ zip ps [lastPacketTime + timeStep, lastPacketTime + timeStep + timeStep .. thisPacketTime]
            in ops

playTimeFunction SourceFileITS (PSPlaying baseTime) _ ss = do
    -- the packet sender
    ops <- readMVar $ psPackets $ rssPacketSender ss
    let fillQueueOnce = do
        canRead <- isReadPossible $ rssFileHandle ss
        if (canRead) then do
            block <- readITSBlock $ rssFileHandle ss
            let thisPacketTime = timeSpecToDouble $ itsTime block
            let ptrs = map (\i -> ((itsTS block) `plusPtr` (i*188),
                                    -- second field of tuple is reference to deallocator:
                                    if i == 6 then destroyITSBlock block else return ()
                                    )) [0..6]
            let opack = OutPacket ptrs (thisPacketTime)
            -- putStrLn $ "playTimeFunction ITS: " ++ show ("basetime=",baseTime,"timeInLocal: ",thisPacketTime - baseTime)
            when trackSequence $ do
                hPutStr stdout "^"
            modifyMVar_ (psPackets $ rssPacketSender ss) (\ops_ -> do
                let q' = que_enqueue (opsPackets ops_) opack
                -- putStrLn $ "TOP="++(showOutPacket $ fromJust $ QC.top q')
                return $ ops_ {
                    opsBaseTime = baseTime,
                    opsPackets = q',
                    opsLastAddedPacket = Just opack
                     })
            return $ ss
          else do
            when trackSequence $ do
                hPutStr stdout "(!r)"
                hFlush stdout
            threadDelay $ 100 * 1000
            return ss
    case outPacketsInterval ops of
        Nothing -> fillQueueOnce
        Just range | range < 1 -> fillQueueOnce
        xxx -> do
            -- queue full, wait 100 msec
            when trackSequence $ do
                --let lenn = que_length $ opsPackets ops
                hPutStr stdout $ "(f"++(show xxx)++")"
                -- hPutStr stdout $ "f"
                hFlush stdout
            threadDelay $ 100 * 1000
            return ss

playTimeFunction _ _ _ _ = error "playTimeFunction pattern matching"


groupPackets (s1:s2:s3:s4:s5:s6:s7:ss)  acc = groupPackets ss ([s1,s2,s3,s4,s5,s6,s7]:acc)
groupPackets [] acc = reverse acc
groupPackets s acc = reverse (s:acc)


streamer sess = do
    keep <- readMVar $ rsActive sess
    if (keep) then do
        ss <- takeMVar $ rsStreaming sess
        playState <- readIORef (rsPlayState sess)
        ss' <- playTimeFunction (rsSourceFileType sess) playState sess ss
        putMVar (rsStreaming sess) ss'
        -- putStrLn $ ("stream until: "++(show (ct, baseTime, streamUntil)))
        streamer sess
      else do
        return ()



sendPackets _ [] _ = return ()
sendPackets seqq packets writeHandle  = {-# SCC "sendPackets" #-} do
    -- ctf <- currentTimeMillisF
    -- print $ "sendPacketsx: currentTime: "++(show ctf)
    outpacket <- constructUDPPacket seqq packets
    catchIOError ( do
        {-# SCC "+hPut" #-} BS.hPut writeHandle outpacket
        hFlush writeHandle
      ) (\e -> print e)
    return ()



readPacketsUntil u acc readHandle = {-# SCC "readPackets" #-} do
    pos <- hTell readHandle
    ptr <- mallocBytes 188
    hGetBuf readHandle ptr 188
    let pkt_= decodePacket ptr ptr (fromEnum (pos `div` 188))
    case pkt_  of
        Just pkt ->
            if (packetIsPast u pkt)
                then return $ reverse (pkt:acc)
                else readPacketsUntil u (pkt:acc) readHandle
        Nothing -> do
            putStrLn $ "bad packet at offset: "++(show 0)
            readPacketsUntil u acc readHandle


