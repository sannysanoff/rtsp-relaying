module MPEG.Server where

import System.IO
import Data.Maybe
import Data.List
import Data.Word
import Data.IORef
import Data.Bits
import Data.Ratio
import qualified Data.IntMap as IM
import qualified Data.Map as M
import qualified Data.ByteString.Lazy as BS
import Data.ByteString.Internal
import Control.Monad
import Control.Applicative
-- import qualified Control.Exception as CE
import Network.Socket
import Text.URI
import System.FilePath.Posix
import System.Directory
import System.Clock
import Data.Time.Clock.POSIX
import Data.Time.Clock
import Data.Time.Format
import Data.Time.LocalTime
import Control.Concurrent
import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import System.IO.Unsafe
import System.IO.Error
import System.Locale
import System.Time
import System.Process
import MPEG.MpegTypes
import Utils.SanUtils
import Codec.MpegTS.Formats
import Codec.MpegTS.IndexFile
import Data.Queue.SimplestQueue
import Net.RTSP.RtspTypes
import CapturingServer.Storage
import Text.Regex
import Text.JSON
import Debug.Trace
import RelayingServer.Relaying hiding (fromJustX, swapBytes)

removeTrackName url =
    trace ( "removeTrackName: "++(url)) $ subRegex expr url ""
    where expr = (mkRegex "/track1$")

urlToFilePath :: String -> String -> String
urlToFilePath url basepath =
    let uri = fromJustX "urlToFilePath" $ parseURI $ removeTrackName url
        tsfile = uriPath uri
    in basepath++tsfile

trackSequence = True

makeStreamSource url True basePath = do
    let uri = fromJustX "makeStreamSource" $ parseURI $ removeTrackName url
        tsfile = uriPath uri
        splitpath =  traceval1 "splitPath=" $ tail $ splitDirectories tsfile
        timestamp = read (splitpath !! 2) :: Int
        storage = ITSStorage (basePath++"/"++(splitpath !! 1))
    putStrLn $ "openForRead "++(show storage)++" ts="++(show timestamp)
    seg <- openForRead storage (TimeSpec timestamp 0)
    segv <- newIORef $ fromJustX "makeStreamSource2" seg
    return $ StorageStreamSource storage segv

makeStreamSource url False basepath = do
        fhandle <- openBinaryFile (urlToFilePath url basepath) ReadMode
        hSetBuffering fhandle $ BlockBuffering (Just 1024000) -- NoBuffering
        return $ HandleStreamSource fhandle

destroyStreamSource (HandleStreamSource fhandle) =
    hClose fhandle

destroyStreamSource (StorageStreamSource _ segv) = do
    (ITSSegment { segHandle = h }) <- readIORef segv
    hClose h
    return ()

sourceToType StorageStreamSource {} = SourceFileITS
sourceToType HandleStreamSource {} = SourceFileTS

getUserKeyFromUrl url =
    let uri = fromJustX "getUserKeyFromUrl" $ parseURI $ removeTrackName url
    in uriQuery uri


mkRTSPSession :: RTSPServer -> HostAddress -> Int -> String -> FilePath -> IO RTSPSession
mkRTSPSession srvr clientIp clientPort url basePath = do
        cnt <- takeMVar $ rsrvSessionIncrement srvr
        putMVar (rsrvSessionIncrement srvr) (cnt+1)
        sock <- socket AF_INET Datagram defaultProtocol
        let clientSockAddr = (SockAddrInet (PortNum $ swapBytes $ toEnum clientPort) clientIp)
        connect sock clientSockAddr
        serverSocketName <- getSocketName sock
        streamSource <- makeStreamSource url (isITSUrl url) basePath
        active <- newMVar True
        socketHandle <- socketToHandle sock WriteMode
        playStateRef <- newIORef PSInitializing
        packetSender <- runSender socketHandle =<< currentTimeMillisF
        streaming <- newMVar (RTSPStreaming 0 0 streamSource 1000 0 0 packetSender 0.02)
        let sourceFileType = sourceToType streamSource
        let userKey = getUserKeyFromUrl url
        when (sourceFileType == SourceFileTS) $
            maybeStartIndexing (urlToFilePath url basePath)
        let newSession = RTSPSession {
                rsClientSockAddr = clientSockAddr,
                rsServerSockAddr = serverSocketName,
                rsURL = url,
                rsNumber = (cnt+1),
                rsSocketHandle = socketHandle,
                rsActive = active,
                rsStreaming = streaming,
                rsUserKey = userKey,
                rsPlayState = playStateRef,
                rsSourceFileType = sourceFileType
                }
        withMaybe userKey $ \uk ->
                modifyMVar_ (rsrvSessionsByUserKey srvr) (return . M.insert uk newSession)
        modifyMVar (rsrvSessions srvr) (\m -> return $ (IM.insert (cnt+1) newSession m, ()))
        return newSession

findFirstPES h = do
    hSeek h AbsoluteSeek 0
    pes <- getPES 10000
    hSeek h AbsoluteSeek 0
    return pes
    where
        getPES :: Int -> IO (Maybe PESPacket)
        getPES 0 = return Nothing
        getPES r = do
                filepos <- fromInteger <$> hTell h
                bs <- BS.hGet h 188
                let pkt = withBSAsPtr bs $ \ptr _ -> decodePacket ptr nullPtr filepos
                if (isJust pkt && (isJust $ pesGetPTS' $ fromJustX "findFirstPES" pkt))
                    then return $ tspPES $ fromJustX "findFirstPES2" pkt
                    else getPES (r  -1)

findLastPES h = do
    fsize <- hFileSize h
    if (fsize `mod` 188) /= 0 then return Nothing
      else do
        let record = fsize `div` 188
        pes <- getPES (record-1) 10000
        hSeek h AbsoluteSeek 0
        return pes
        where
            getPES :: Integer -> Int -> IO (Maybe PESPacket)
            getPES (-1) _ = return Nothing
            getPES _ 0 = return Nothing
            getPES pos r = do
                    hSeek h AbsoluteSeek (pos * 188)
                    bs <- BS.hGet h 188
                    let pkt = withBSAsPtr bs $ \ptr _ -> decodePacket ptr nullPtr (fromInteger $ pos * 188)
                    if isJust $ tspPES $ fromJustX "findLastPes(1)" pkt
                        then return $ tspPES $ fromJustX "findLastPes(2)" pkt
                        else getPES (pos-1) (r  -1)


mkRTSPServer = do
        increment <- newMVar 0
        sessions <- newMVar $ IM.empty
        sessionsByUserKey <- newMVar $ M.empty
        return $ RTSPServer {
                        rsrvSessionIncrement = increment,
                        rsrvSessions = sessions,
                        rsrvSessionsByUserKey = sessionsByUserKey
                        }



getSession srvr sessNo = do
    amap <- readMVar $ rsrvSessions srvr
    return $ IM.lookup sessNo amap

getSessionByUserKey _ Nothing = return Nothing
getSessionByUserKey srvr (Just userKey) = do
    amap <- readMVar $ rsrvSessionsByUserKey srvr
--    print $ "gsbuk: "++(show amap)
    let retval = M.lookup userKey amap
    return retval

releaseSession session srvr = do
    oldActive <- modifyMVar (rsActive session) (\active -> return (False, (active)))
    when (oldActive) $ do
        streaming <- readMVar (rsStreaming session)
        writeIORef (psAlive $ rssPacketSender streaming) False
        destroyStreamSource $ rssFileHandle streaming
        hClose $ rsSocketHandle session
        modifyMVar (rsrvSessions srvr) (\m -> return (IM.delete (rsNumber session) m, ()))
        withMaybe (rsUserKey session) $ \uk -> (modifyMVar_ (rsrvSessionsByUserKey srvr) $ return . M.delete uk)
        print "released session"
    return()

prepareStreaming :: RTSPSession -> IO (Word16, Word32)
prepareStreaming sess = do
    tm <- currentTimeMillis
    let retval = (300, fromIntegral tm)
    modifyMVar_ (rsStreaming sess) (\v ->
                    return $ v { rssSeq = fst retval,
                        rssStreamedPos = 1000,
                        rssRtpTime = snd retval });
    return retval

streamPREBUFFER_SEC _ = 0.0 -- if t < 1 then 0.2 else 0

withSession maybeSession ho cseq rest = do
    case (maybeSession) of
        Just session -> do
            rest session
        Nothing -> do
            hPutStr ho $ concat $ intersperse "\r\n" $ [
                            "RTSP/1.0 404 NOT FOUND!",
                            "CSeq: "++cseq,
                            "",""]
    hFlush ho

rtspGetParameter maybeSession ho cseq  = do
    withSession maybeSession ho cseq $ \session -> do
            let sessionNo = rsNumber session
            hPutStr ho $ concat $ intersperse "\r\n" $ [
                            "RTSP/1.0 200 OK",
                            "CSeq: "++cseq,
                            "Session: "++(show sessionNo),
                            "",
                            ""
                            ]

make200Response text =
    concat $ intersperse "\r\n" $ [
                        "HTTP/1.1 200 OK",
                        "Content-Type: text/plain",
                        "",
                        text
                        ]

htmlGetTimeShiftingDiff maybeSession ho = do
    withSession maybeSession ho "-1" $ \sess -> do
        (RTSPStreaming {rssPacketSender = sender})  <- readMVar $ rsStreaming sess
        (OutPackets _ q baseTime _) <- readMVar $ psPackets sender
        ctm <- currentTimeMillisF
        let (OutPacket {opTimeInBase = tib}, _) = que_dequeue q
        print ("ctm",ctm,"baseTime",baseTime,"pktoff",tib)
        -- hPutStr ho $ make200Response $ "{timeOffset: "++(show $ - (ctm - (baseTime + tib)))++"}"
        hPutStr ho $ make200Response $ "{timeOffset: "++(show $ -baseTime)++"}"

htmlGetStatus ho storage basedir = do
    status <- storageGetStatus True $ ITSStorage $ basedir ++ "/"++storage
    ctm <- currentTimeMillis;
    print status
    hPutStr ho $ make200Response $
            encode $ toJSObject [
                    ("alive",JSBool $ stosCurrentlyActive status),
                    ("serverTime",JSString $ toJSString $ show ctm),
                    ("quantum",JSRational True $ (toInteger secondsPerFile) % 1 ),
                    ("segments", JSArray $ map toJSON $ stosSegments status)]
    where
        toJSON :: ITSSegmentStatus -> JSValue
        toJSON (ITSSegmentStatus start end) = JSArray [
                    JSString $ toJSString $ posix2dt start,
                    JSString $ toJSString $ posix2dt end,
                    JSString $ toJSString $ show start,
                    JSString $ toJSString $ show end
                    ]


htmlGetProgramFiles ho ts leng storage basedir = do
        status <- storageGetStatus False $ ITSStorage $ basedir ++ "/"++storage
        hPutStr ho $ make200Response $
            encode $ toJSObject [
                    ("segments", JSArray $ map toJSON $ filter (segmentContainsProgram) $ stosSegments status)]
    where
        segmentContainsProgram (ITSSegmentStatus start end) =
            traceval1 (show ("segment",start,end,"whether contains",ts,leng)) $ not (start > ts + leng || end < ts)
        toJSON :: ITSSegmentStatus -> JSValue
        toJSON (ITSSegmentStatus start _) = JSString $ toJSString $ show start

htmlPinFile ho args basedir = do
    let subStorage = fromJustX "htmlPinFile.1" $ lookup  "channel" args
    let tsS = fromJustX "htmlPinFile.2" $ lookup  "ts" args
    let storage = ITSStorage $ basedir ++ "/"++subStorage
    let ts = TimeSpec (read tsS :: Int) 0
    (result,desc) <- storagePinFile storage ts
    hPutStr ho $ make200Response $ (if result then "OK\n"++desc else "FAIL\n"++desc)

htmlUnpinFile ho args basedir = do
    let subStorage = fromJustX "htmlPinFile.1" $ lookup  "channel" args
    let tsS = fromJustX "htmlPinFile.2" $ lookup  "ts" args
    let storage = ITSStorage $ basedir ++ "/"++subStorage
    let ts = TimeSpec (read tsS :: Int) 0
    (result,desc) <- storageUnpinFile storage ts
    hPutStr ho $ make200Response $ (if result then "OK\n"++desc else "FAIL\n"++desc)

htmlGetAllBroadcastStatus ho basedir = do
    filez <- filter (endsWith "timestamp") <$> getDirectoryContents basedir
    modTimes <- mapM (\f -> utcTimeToPOSIXSeconds <$> getModificationTime (basedir ++ "/" ++ f)) filez
    let result = concat $  map  (\(f,posixsec) -> removeSuffix f ++ " " ++ (show posixsec) ++ "\n") $ zip filez modTimes
    hPutStr ho $ make200Response $ result
    where removeSuffix  = reverse . drop 10 . reverse

htmlDescribeStream ho args = do
    let stream = fromJustX "htmlDescribeStream.args" $ lookup  "stream" args
    case parseURI stream of
        Just _ -> do
            buffer <- mkCircularBuffer 100000
            items <- probeStream stream buffer 3
            let itemsS = "["++(concat $ intersperse "," $ map show items)++"]"
            hPutStr ho $ make200Response $ "{ids: "++itemsS++"}"
        Nothing ->
            hPutStr ho $ make200Response $ "{error: 'INVALID_URL'}"

currentTz = unsafePerformIO $ getCurrentTimeZone

-- input: 12709093309
-- output 201012312359
posix2dt posix =
    let utctime = posixSecondsToUTCTime ( realToFrac posix :: NominalDiffTime)
        (ZonedTime localtime _) = utcToZonedTime currentTz utctime
    in formatTime defaultTimeLocale "%Y%m%d%H%M" localtime



rtspPause maybeSession ho cseq pauseStreaming = do
    withSession maybeSession ho cseq $ \session -> do
        let sessionNo = rsNumber session
        pauseStreaming (rsSourceFileType session) session
        hPutStr ho $ concat $ intersperse "\r\n" $ [
                        "RTSP/1.0 200 OK",
                        "CSeq: "++cseq,
                        "Session: "++(show sessionNo),
                        "",
                        ""
                        ]

rtspStartPlay maybeSession ho cseq range startStreaming continueStreaming = do
    withSession maybeSession ho cseq $ \session -> do
        let sessionNo = rsNumber session
        ps <- readIORef (rsPlayState session)
        case ps of
            PSInitializing -> do
                (seqq, rtptime) <- prepareStreaming session
                hPutStr ho $ concat $ intersperse "\r\n" $ [
                                "RTSP/1.0 200 OK",
                                "CSeq: "++cseq,
                                "Range: "++range,
                                "Session: "++(show sessionNo),
                                "RTP-Info: "++
                                    "(url="++(rsURL session)++";"++
                                    "seq="++(show seqq)++";"++
                                    "rtptime="++(show rtptime),
                                "",
                                ""
                                ]
                startStreaming (rsSourceFileType session) session
            PSPlaying _ -> do
                hPutStr ho $ concat $ intersperse "\r\n" $ [
                                "RTSP/1.0 500 ALREADY_PLAYING",
                                "CSeq: "++cseq,
                                "Session: "++(show sessionNo),
                                "", ""
                                ]
            PSPaused -> do
                continueStreaming (rsSourceFileType session) session
                hPutStr ho $ concat $ intersperse "\r\n" $ [
                                "RTSP/1.0 200 OK",
                                "CSeq: "++cseq,
                                "Session: "++(show sessionNo),
                                "", ""
                                ]

isITSUrl url =
    containsText "/replay/" url


rtspDescribe url ho cseq basePath = do
    sdpResponse <- if isITSUrl url then rtspDescribeTS else rtspDescribeITS
    -- streamSource <- makeStreamSource url (isITSUrl url) basePath
    hPutStr ho $ concat $ intersperse "\r\n" $ [
                "RTSP/1.0 200 OK",
                "CSeq: "++cseq,
                "Content-Base: "++url++"/",
                "Content-Type: application/sdp",
                "Content-Length: "++(show $ length sdpResponse),
                "",
                sdpResponse]
    where
      rtspDescribeTS = do
        let fpath = urlToFilePath (url++"/stream1_or_so") basePath
        (start,stop) <- do
            exists <- doesFileExist fpath
            if (exists) then do
                index <- openIndex fpath
                let nelems = getIndexSize index
                (start,_) <- getIndexEntry 0 index
                print ("start",start)
                print ("nelems",start)
                (stop,_) <- getIndexEntry (nelems-1) index
                print ("stop",stop)
                closeIndex index
                return (start,stop)
              else return (0, 100)
        putStrLn $ "rtsp describe!"++(show url)++" range=0-"++(show $ stop - start)
        return $ concat $ intersperse "\r\n" $ [
                "v=0",
                "o=- 1269219238252055 1 IN IP4 192.168.1.77",
                "s=MPEG Transport Stream, streamed by san and his haskell",
                "i=sample.ts",
                "t=0 0",
                "a=tool:san's hands",
                "a=type:broadcast",
                "a=control:*",
                "a=range:npt=0-"++(show $ stop-start),
                "m=video 0 RTP/AVP 33",
                "c=IN IP4 0.0.0.0",
                "a=control:track1"
                ]
      rtspDescribeITS = do
        putStrLn $ "rtsp describe ITS!"
        return $ concat $ intersperse "\r\n" $ [
                "v=0",
                "o=- 1269219238252055 1 IN IP4 192.168.1.77",
                "s=MPEG Transport Stream, streamed by san and his haskell",
                "i=sample.ts",
                "t=0 0",
                "a=tool:san's hands",
                "a=type:broadcast",
                "a=control:*",
--                "a=range:npt=0-"++(show $ stop-start),
                "m=video 0 RTP/AVP 33",
                "c=IN IP4 0.0.0.0",
                "a=control:track1"
                ]


getLookAhead _ = 0
{-
    | isVideo streamId = 0
    | isAudio streamId = -0.2
    -}

readPackets 0 acc _ = return $ reverse acc
readPackets n acc readHandle = do
    pos <- hTell readHandle
    ptr <- mallocBytes 188
    hGetBuf readHandle ptr 188
    let pkt_ = decodePacket ptr ptr (fromEnum (pos `div` 188))
    case pkt_  of
        Just pkt -> readPackets (n-1) (pkt:acc) readHandle
        Nothing -> do
            putStrLn $ "bad packet at offset: "++(show pos)
            readPackets (n-1) acc readHandle

-- returns reverse Packets, and most recent packet contains TS
readPacketsUntilTS acc readHandle = do
    pos <- hTell readHandle
    ptr <- mallocBytes 188
    hGetBuf readHandle ptr 188
    -- newBS <- BS.hGet readHandle 188
    let pkt_ = decodePacket ptr ptr (fromEnum (pos `div` 188))
    case pkt_  of
        Just pkt -> do
            if isJust $ getPacketTS pkt then
                return $ pkt:acc
             else readPacketsUntilTS (pkt:acc) readHandle
        Nothing -> do
            free ptr
            putStrLn $ "bad packet at offset: "++(show pos)
            readPacketsUntilTS acc readHandle

constructUDPPacket seqq packets = do
    catchIOError ( do
    ctm <- currentTimeMillis
    let curTime = fromIntegral $ ctm * 90 .&. 0xFFFFFFFF
    let allocSize = 2 + 2 + 4 + 4 + 188 * (length packets)
    fullbs <- create allocSize $ \ptr -> do
        poke (ptr `plusPtr` 0) (0x80 :: Word8)
        poke (ptr `plusPtr` 1) (0x21 :: Word8)
        word16ToBytesPtr seqq (ptr `plusPtr` 2)
        word32ToBytesPtr curTime (ptr `plusPtr` 4)
        poke (ptr `plusPtr` 8) (0x12 :: Word8)
        poke (ptr `plusPtr` 9) (0x34 :: Word8)
        poke (ptr `plusPtr` 10) (0x56 :: Word8)
        poke (ptr `plusPtr` 11) (0x78 :: Word8)
        mapM_ (\((pktptr,freer), dest) -> do
                    memcpy (ptr `plusPtr` dest) pktptr 188
                    freer
                    ) $ zip packets [12, 12+188 ..]

    return $ BS.fromChunks [fullbs]
    ) (\e -> error $ "constructUDPPacket: exception: "++(show e))

runSender :: Handle -> Double -> IO PacketSender
runSender oh baseTime_ = do
    packets <- newMVar $ OutPackets Nothing que_empty baseTime_ False
    alive <- newIORef True
    seqq <- newIORef 123
    let ps = PacketSender oh packets seqq alive
    let timerProc = do
        op@(OutPackets _ q basetime paused) <- {-# SCC "mvarPackets" #-}  takeMVar packets
        -- putStrLn $ "takeTime="++(show $l2-l1)
        if (que_isempty q || paused) then do
            -- no packets
            putMVar packets op
            -- putStrLn "SENDER: idle"
            threadDelay 10000
          else do
            -- some packets
            let (pkt, q') = que_dequeue q
            currentTimeLocal <- currentTimeMillisF
            let delay = opTimeInBase pkt - (currentTimeLocal - basetime)
            putMVar packets $ op {opsPackets =  q' }
            -- putStrLn $ "SENDER: wait "++(show delay)
            when (delay > 0) $ do
                when trackSequence $ do
                    -- hPutStr stdout $ "(D" ++ (show delay)++")"
                    hPutStr stdout $ "D"
                {-# SCC "threadDelay" #-} threadDelay $ round (delay * 1000000)
                -- already time to send
            seqq_ <- {-# SCC "iorefs" #-} readIORef $ psSequence ps
            {-# SCC "iorefs" #-} writeIORef (psSequence ps) (seqq_+1)
            udpPacket <- constructUDPPacket seqq_ $ opData pkt
            catchIOError ( do
                BS.hPut oh udpPacket
                hFlush oh
                ) (\e -> print e)
            when trackSequence $ do
                hPutStr stdout ">"
        alive' <- readIORef $ psAlive ps
        if (alive') then timerProc else return ()
    forkIO timerProc
    return ps

listInterfaces = "ifconfig| grep 'inet '| sed 's/addr://g'| awk '{print $2}'| grep -v 127.0.0.1"
getInterfaces = lines <$> readProcess "sh" ["-c", listInterfaces] ""


probeStream url buffer@(CircularBuffer bufSize buf posSeqVar _ _) seconds = do
    interfaces <- getInterfaces
    let uri = fromJustX "parseURI" $ parseURI $ url
    let hu = case uriScheme uri of
            Just "http" -> (url,"")
            Just "udp" -> ("", url)
            _ -> error "invalid URL scheme, http or udp only"
    tid <- forkIO $ receiver (fst hu) (snd hu) buffer Nothing interfaces False defaultUserAgent ""
    threadDelay $ seconds * 1000000
    PosAndSeq pos seqq <- readIORef posSeqVar
    when (seqq == 0) $ error "RESULT: NO_DATA"
    let limit = if (seqq > pos) then bufSize else pos
    killThread tid
    let packets = catMaybes $ map (\i -> decodePacket (buf `plusPtr`  (i * 188)) (buf `plusPtr` i) 0) [0..limit]
    return $ nub $ map tspPID $ packets
