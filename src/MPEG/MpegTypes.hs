module MPEG.MpegTypes where


import Data.Word
--import Data.ByteString.Lazy
import Foreign.Ptr
import Foreign.ForeignPtr
import qualified Data.IntMap as IM
import qualified Data.Map as M
import Control.Concurrent.MVar

import Net.RTSP.RtspTypes

data TransportWord = TWOther String | TWUnicast | TWClientPort Int Int | TWError String
                        | TWServerPort Int | TWDestination String | TWSource String
                        | TWModePlay
                                deriving (Eq)

instance Show TransportWord where
        show (TWError _) = "xxxxx"
        show (TWOther str) = str
        show (TWSource str) = "source="++str
        show (TWDestination addr) = "destination="++(addr)
        show (TWUnicast) = "unicast"
        show (TWModePlay) = "mode=\"PLAY\""
        show (TWClientPort f t) = "client_port="++(show f)++"-"++(show t)
        show (TWServerPort f) = "server_port="++(show f)++"-"++(show (f+1))

data PMTStream = PMTStream {
                    pmtsStreamType :: Int,
                    pmtsPID :: Int
                } deriving Show

data PESPacket = PESPacket {
                pesStreamID :: !Int,
                pesDTS :: !(Maybe Double),
                pesPTS :: !(Maybe Double),
                pesFrameType :: !(Maybe FrameType),
                pesInd0 :: !Int
        } | PATPacket {
                patTableId :: Int,
                patTransportStreamId :: Int,
                patCurrentOrNext :: Bool, -- current = True
                patSectionNumber :: Int,
                patLastSectionNumber :: Int,
                patPrograms :: [Either Int Int]
        } | PMTPacket {
                pmtTableId :: Int,
                pmtProgramNumber :: Int,
                pmtPCR_PID :: Int,
                pmtStreams :: [PMTStream]
        } deriving Show

data FrameType = IFrame | PFrame | BFrame deriving Show

data TSPacket = TSPacket {
    tspPID :: !Int,
    tspPES :: Maybe PESPacket,
    tspDataPtr :: Ptr Word8,
    tspFileOffset :: !Int
        } deriving Show


data RTSPServer = RTSPServer {
    rsrvSessionIncrement :: MVar Int,
    rsrvSessions :: MVar (IM.IntMap RTSPSession),
    rsrvSessionsByUserKey :: MVar (M.Map String RTSPSession)
}

data OpenIndex = OpenIndex {
        oiPtr :: ForeignPtr Word8,
        oiOffset :: Int,
        oiRawSize :: Int
}

isPATPacket (Just (TSPacket {tspPES = Just PATPacket {}})) = True
isPATPacket _ = False
