-----------------------------------------------------------------------------
--
-- Module      :  CapturingServer.Storage
-- Copyright   :  San
-- License     :  AllRightsReserved
--
-- Maintainer  :  San
-- Stability   :  provisional
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module CapturingServer.Storage where

import Data.Word
import Data.Bits
import Data.Maybe
import qualified Data.Map as M
import Data.List
import System.IO
import System.Clock
import System.Directory
import Foreign.Marshal.Alloc
import Foreign.Storable
import Foreign.Ptr
import Control.Applicative
import Control.Concurrent.MVar
import Utils.SanUtils
import System.IO.Unsafe
import System.IO.Error
import System.FilePath
import System.Posix.Files

data ITSSegmentStatus = ITSSegmentStatus {
    sesStart :: Int,
    sesLast :: Int
} deriving (Show,Eq,Ord)

data ITSStorageStatus = ITSStorageStatus {
    stosCurrentlyActive :: Bool,
    stosSegments :: [ITSSegmentStatus]
} deriving Show

data ITSStorage = ITSStorage {
     stPath :: FilePath
     -- stHandlez :: MVar (M.Map Handle FilePath)
     } deriving Show

data ITSSegment = ITSSegment {
    segHandle :: Handle,
    segStart :: Int,            -- pure seconds with some offset
    segStorage :: ITSStorage
    } deriving Show



secondsPerFile = 4000

entrySize = 188 * 7 + 4 + 4 :: Word64

-- returns start of segment for given timespec
-- input: 380 sec output: 300 sec (if files contain 100 sec per file)
segmentStart (TimeSpec secs _) = (secs `div` secondsPerFile) * secondsPerFile

-- if given segment contains data for given timestamp
segmentContains seg ts = segStart seg == segmentStart ts

-- creates segment filename for given ts
segmentFileName ts =
    let start = segmentStart ts
    in "capture" ++ (show start) ++ ".its"

segmentClose seg =
    hClose (segHandle seg)

openForAppend stor ts = do
    let fn = (stPath stor) ++ "/" ++ (segmentFileName ts)
    fh <- openBinaryFile fn ReadWriteMode
    print $ "opened for append: "++(fn)
    size <- fromInteger <$> hFileSize fh
    hSeek fh AbsoluteSeek $ toInteger (entrySize * (size `div` entrySize))   -- override incomplete record)
    return $ ITSSegment fh (segmentStart ts) stor

maybeSwitchSegmentWhileWriting stor ts seg  =
    if (isJust seg) then
        if (segmentContains (fromJustX "maybeSwitchSegmentWhileWriting" seg) ts) then return seg
        else do
            segmentClose (fromJustX "maybeSwitchSegmentWhileWriting2" seg)
            nseg <- openForAppend stor ts
            return $ Just nseg
      else do
        nseg <- openForAppend stor ts
        return $ Just nseg

maybeSwitchSegmentWhileReading seg = do
    eof <- hIsEOF (segHandle seg)
    if eof then do
        hClose (segHandle seg)
        let newStart = TimeSpec (segStart seg + secondsPerFile) 0
        fn <- savedOrTemporaryFile (stPath $ segStorage seg) (segmentFileName newStart)
        fh <- openBinaryFile fn ReadMode
        print $ "opened for read: "++(fn)
        return $ ITSSegment fh (segmentStart newStart) (segStorage seg)
      else
        return $ seg

savedOrTemporaryFile basePath fileName = do
        let fn = basePath ++ "/" ++ fileName
        _ <- doesFileExist fn
        let fn' = basePath ++ "/saved/" ++ fileName ++ ".saved"
        exists2 <- doesFileExist fn'
        if (exists2) then return fn' else return fn


canReadMore seg = do
    eof <- hIsEOF (segHandle seg)
    if eof then do
        let newStart = TimeSpec (segStart seg + secondsPerFile) 0
        fn <- savedOrTemporaryFile (stPath $ segStorage seg) (segmentFileName newStart)
        exists <- doesFileExist fn
        return exists
      else
        return True

openForRead s ts = do
    fn <- savedOrTemporaryFile (stPath s) (segmentFileName ts)
    exists <- doesFileExist fn
    putStrLn $ "openForRead: file "++(fn)++" exists: "++(show exists)
    if exists then do
        fh <- openBinaryFile fn ReadMode
        size <- fromInteger <$> hFileSize fh
        print "start seek"
        seekInSegment fh ts 0 (size `div` entrySize)
        print "finish seek"
        return $ Just $ ITSSegment fh (segmentStart ts) s
      else do
    putStrLn $ "%%%% file does not exist: " ++ (fn)
    return Nothing

storagePinFile s ts = do
    fn <- savedOrTemporaryFile (stPath s) (segmentFileName ts)
    exists <- doesFileExist fn
    if (endsWith ".its" fn) then do
        if (exists) then do
            let dir = takeDirectory fn
            let fname = takeFileName fn
            let newfn = dir ++ "/saved/" ++ fname ++ ".saved"
            createDirectoryIfMissing True $ dir ++ "/saved"
            createLink fn newfn             -- hard link
            return (True,"success")         -- successfully linked
         else return (False,"Source file not found: "++fn)      -- file not found!
      else return (True,"already pinned")     -- already returned saved file.

storageUnpinFile s ts = do
    fn <- savedOrTemporaryFile (stPath s) (segmentFileName ts)
    if (endsWith ".its.saved" fn) then do
        removeFile fn
        return (True,"Removed "++fn)
      else return (False,"File not found")     -- file was not found

timeSpecToInt64 :: TimeSpec -> Word64
timeSpecToInt64 (TimeSpec s ns) =
    (fromIntegral s) * 1000000000 + fromIntegral ns

getEntryIndex :: Handle -> Int -> IO Word64
getEntryIndex fh index = do
    hSeek fh AbsoluteSeek $ toInteger index
    alloca $ \ptr1 ->
        alloca $ \ptr2 -> do
            hGetBuf fh (ptr1 :: Ptr Int) 4
            hGetBuf fh (ptr2 :: Ptr Int) 4
            s <- peek ptr1
            ns <- peek ptr2
            return $ (fromIntegral s) * 1000000000 + fromIntegral ns

seekInSegment :: Handle -> TimeSpec -> Word64 -> Word64 -> IO Bool
seekInSegment fh ts lo hi = do
        -- have we arrived?
        print ("seekInSegment",ts,lo,hi,sec ts)
        if (abs (lo - hi) < 2) then do
            -- is time ok?
            ts1 <- peekSegment fh lo
            if (abs (sec ts1 - sec ts) < 2) then do
                -- can we seek OK to that place?
                let reqPos = fromIntegral $ entrySize * lo
                hSeek fh AbsoluteSeek reqPos
                currPos <- hTell fh
                let retval = (currPos == reqPos)
                print $ "seek completed, retval="++(show (retval, currPos))
                return retval
              else do
                -- time is not ok
                return False
         else do
            -- find the middle position
            let splitPos = ((lo + hi) `div` 2)
            -- and its timestamp
            splitTs <- peekSegment fh splitPos
            print ("splitTs",splitTs)
            if (sec splitTs == sec ts) then return True
                else if splitTs < ts then do
                    -- seek in upper half
                    seekInSegment fh ts splitPos hi
                else do
                    -- seek in lower half
                    seekInSegment fh ts lo splitPos

peekSegment :: Handle -> Word64 -> IO TimeSpec
peekSegment fh ix = do
    -- seek to proper place
    hSeek fh AbsoluteSeek $ toInteger $ ix * entrySize
    -- allocate memory
    alloca $ \ptr1 ->
        alloca $ \ptr2 -> do
            -- read
            hGetBuf fh ptr1 4
            hGetBuf fh ptr2 4
            secx <- peek ptr1 :: IO Int
            nsecx <- peek ptr2 :: IO Int
            -- stay at position
            hSeek fh AbsoluteSeek $ toInteger $ ix * entrySize
            -- convert
            return $ TimeSpec (secx .&. 0xFFFFFFFF) (nsecx .&. 0xFFFFFFFF)

storageStatusCache = unsafePerformIO $ newMVar $ M.empty

storageGetStatus merge (ITSStorage path)  = do
    filez <- catchIOError (filter (endsWith ".its") <$> getDirectoryContents path) (\_ -> return [])
    segs <- ((if merge then mergeSegments [] else id) . sort . catMaybes)  <$> mapM (\fn -> do
        statusCache <- readMVar storageStatusCache
        putStrLn $ "checking file: "++show (path,fn)
        case (M.lookup (path,fn) statusCache) of
            Nothing -> do
                h <- openBinaryFile (path ++ "/" ++ fn) ReadMode
                nEntries <- flip div entrySize <$> fromInteger <$> hFileSize h
                putStrLn $ "not in cache, nEntries = "++(show nEntries)
                if (nEntries > 0) then do
                    print $ "entries: "++(show nEntries)
                    ts1 <- peekSegment h 0
                    ts2 <- peekSegment h (nEntries-1)
                    hClose h
                    let val = ITSSegmentStatus (sec ts1) (sec ts2)
                    if (abs (sec ts2 - sec ts1 - secondsPerFile) < 5) then do
                        putStrLn $ "saving to cache: " ++ show (path,fn, val)
                        modifyMVar_ storageStatusCache (return . M.insert (path,fn) val)
                     else do
                        putStrLn $ "avoiding cache: " ++ show (path,fn, val)
                        return ()
                    return $ Just val
                 else return Nothing
            Just val -> do
                putStrLn "from cache"
                return $ Just val
        ) filez
    tm <- getTime Realtime
    -- print ("storage status",path,segs,"sesLast: ",sesLast $ last segs, "realtime: ",sec tm)
    return $ ITSStorageStatus (segs /= [] && (sec tm) - (sesLast $ last segs) < 5) segs


mergeSegments acc (a:b:x) =
    if (sesStart b - sesLast a < 2) then
        mergeSegments acc ((ITSSegmentStatus (sesStart a) (sesLast b)):x)
     else mergeSegments (a:acc) (b:x)
mergeSegments acc [x] = reverse (x:acc)
mergeSegments acc [] = reverse (acc)

