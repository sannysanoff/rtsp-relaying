{-# OPTIONS -XForeignFunctionInterface #-}
{-# OPTIONS -XScopedTypeVariables #-}
{-# OPTIONS -XDeriveDataTypeable #-}
{-# OPTIONS -XBangPatterns #-}
-- vim: sw=4:ts=4:expandtab
-----------------------------------------------------------------------------
--
-- Module      :  Main.Server
-- Copyright   :
-- License     :  AllRightsReserved
--
-- Maintainer  :
-- Stability   :
-- Portability :
--
-- |
--
-----------------------------------------------------------------------------

module Main where

import Network.Socket
import Data.Bits
import Data.Word
import Data.IORef
import Data.Time.Clock.POSIX
import Data.Time.Clock
import qualified Data.Map as M
import Data.Maybe
import Text.Regex
import Control.Applicative
import Control.Monad
import Control.Monad.IfElse
import Control.Concurrent
import System.IO
import System.Directory
import System.Timeout
import System.Clock
-- import System.Posix.Types
import System.Posix.Files
import System.Posix.IO
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.C.Types
import Foreign.Storable
import System.Console.CmdArgs
import System.IO.Unsafe
import CapturingServer.Storage
import Utils.SanUtils (withMaybe, endsWith, parseIP)
import MPEG.MpegTs (timeSpecToDouble)


defaultPortNumber = 1234

swapBytes i =
        let lo = (i .&. 0xFF) `shiftL` 8
            hi = i `shiftR` 8
        in lo .|. hi

currentTimeMillisF = do
        (TimeSpec a b) <- getTime Realtime
        let millis = a * 1000 + (b `div` 1000000)
        return $ (fromIntegral millis) / (1000.0 :: Double)

currentTimeMicros :: IO Int
currentTimeMicros = do
        (TimeSpec a b) <- getTime Realtime
        let micros = (a `mod` 1000) * 1000000 + (b `div` 1000)
        return micros

data ToSave = ToSave {
    tsTime :: !TimeSpec,
    tsData :: !(Ptr Word8)
    }

data Channel = Channel {
    chChan :: !(Chan ToSave),
    chCountInChan :: !Int,
    chLastActivity :: !TimeSpec
    }

mkChannel :: IO Channel
mkChannel = do
    cha <- newChan
    ctm <- getTime Realtime
    return $ Channel cha 0 ctm

data Config = Config { cfgBindInterface :: String,
                        cfgMulticastAddr :: String,
                        cfgPort :: Int,
                        cfgMulticast :: Bool,
                        cfgDaysToKeep :: Int,
                        cfgCaptureImmediateFileOnce :: String,
                        cfgCheckTrafficOnly :: Bool,
                        cfgSpoolDirectory :: String } deriving (Show, Data, Typeable)

{-- Dies ist eine globale Variable!!!!! -}
config  = unsafePerformIO $ cmdArgsRun $ cmdArgsMode $ Config {
                                                cfgBindInterface = "192.168.1.77" &= help "local interface to use for incoming multicast stream" &= explicit &= name "i",
                                                cfgMulticastAddr = "233.3.3.3" &= help "multicast address" &= name "a" &= explicit,
                                                cfgPort = (1234 :: Int) &= help "multicast port" &= name "p" &= explicit,
                                                cfgDaysToKeep = (-1 :: Int) &= help "days to keep recoreded files" &=  name "k" &= explicit,
                                                cfgMulticast = def &= help "specify to join multicast" &= name "j" &= explicit,
                                                cfgCheckTrafficOnly = def &= help "check traffic only (don't store)" &= name "n" &= explicit,
                                                cfgCaptureImmediateFileOnce = "" &= help "[default none] file to capture immediately to (once) instead of normal job" &= name "f" &= explicit,
                                                cfgSpoolDirectory = "/home/spool/iptv-save" &= help "directory for spool files" &= name "d" &= explicit
                                                } &= summary "IPTV grab/relay, version 0.1e-235"

-- directory = root directory, directory/10.2.3.4/files.ts are to clear
cleanupper :: String -> Int -> IO ()
cleanupper directory days = do
    -- print "cleanupper!"
    getDirectoryContents directory >>= mapM_ (\f -> do
            let fullPath = directory ++"/"++f
            whenM (isDirectory <$> getFileStatus fullPath) $ getDirectoryContents fullPath >>= mapM_ (\f2 -> do
                    let fullPath2 = fullPath ++"/"++f2
                    -- print fullPath2
                    whenM (return (endsWith ".its" f2) &&^ (older days =<< modificationTime <$> getFileStatus fullPath2) ) $ do
                        putStrLn $ "removeFile "++ fullPath2
                        removeFile fullPath2
                        threadDelay $ 5 * 1000000 -- 5 sec
                    return ()
                ))
    threadDelay $ 5 * 60 * 1000000   -- 5 min
    cleanupper directory days

older days ctime = do
    let utcthen = posixSecondsToUTCTime $ (realToFrac ctime :: POSIXTime)
    utcnow <- getCurrentTime
    let dt = diffUTCTime utcnow utcthen
    -- putStrLn $ "dt="++(show $ realToFrac dt)
    return $ (realToFrac dt ) > ((fromInteger . toInteger) days) * (24*60*60)

cast :: (Integral a, Integral b) => a -> b
cast = fromInteger . toInteger

-- saver ::
saver nChan channel currentSegment storage fd jm = do
    lst <- readChanFully (chChan nChan) []
    let !lstLen = length lst
    (Channel {chLastActivity = lastActivity}) <- modifyMVar channel $ \ch -> do
        print (chCountInChan ch, fd, if cfgCheckTrafficOnly config then "monitoring:"++show (chLastActivity ch) else "saving")
        when (chCountInChan ch == 0 && not (cfgCheckTrafficOnly config)) $ do
            print $ "rejoin channel "++(show fd)
            jm
            return ()
        return (ch { chCountInChan = (chCountInChan ch - lstLen) }, ch)
    -- putStr $ (show $ length lst) ++ "l ";  hFlush stdout
    let fileTime = fromIntegral $  round $ timeSpecToDouble lastActivity
    let timestampPath = (stPath storage ++ ".timestamp")
    timeStampExists <- fileExist timestampPath
    when (not timeStampExists) $ do
        createFile timestampPath 0x40 {- O_CREAT -} >>= closeFd --
    setFileTimes timestampPath fileTime fileTime
    mapM_ (\ts -> do
        let ptr = tsData ts
        -- print ("tstime=",tsTime ts)
        maybeSeg0 <- readIORef currentSegment
        maybeSeg <- maybeSwitchSegmentWhileWriting storage (tsTime ts) maybeSeg0
        writeIORef currentSegment maybeSeg
        writeToFile (segHandle $ fromJust maybeSeg) (tsTime ts) ptr
        mustBe47 <- peek ptr
        when (mustBe47 /= 0x47 && mustBe47 /= 65) $ putStrLn "Wrong packet (0x47 missing)"
        free ptr) lst
    threadDelay $ 1000 * 1000
    saver nChan channel currentSegment storage fd jm

receiver channelMapVar receiveSocket = do
    -- putStr "."; hFlush stdout;
    channelMap <- readMVar channelMapVar
    ptr <- mallocBytes 1500
    (cnt,addr) <- recvBufFrom receiveSocket ptr 1500
    time <- getTime Realtime
    if (cnt == 7 * 188) then do
        case M.lookup addr channelMap of
            Just channel -> do
                modifyMVar_ channel $ \(ch :: Channel) -> do
                                        -- notifyChan (chChan ch)
                                        if (cfgCheckTrafficOnly config) then do
                                            free ptr
                                            return ch { chLastActivity = time }
                                          else do
                                            let toSave = ToSave time ptr
                                            writeChan (chChan ch) toSave
                                            return $ ch {
                                                chCountInChan = (chCountInChan ch + 1),
                                                chLastActivity = time
                                                }
            Nothing -> free ptr
      else free ptr
    receiver channelMapVar receiveSocket

enableReusePort sock = do
    let fd = cast $ fdSocket sock
    enableReusePortC fd

joinMulticast :: CInt -> CInt -> CInt -> IO CInt
joinMulticast sock destaddr ifaddr = do
  c_joinMulticast sock destaddr ifaddr

leaveMulticast :: CInt -> CInt -> CInt -> IO CInt
leaveMulticast sock destaddr ifaddr = do
  c_leaveMulticast sock destaddr ifaddr


foreign import ccall "enableReusePort" enableReusePortC :: CInt -> IO CInt
foreign import ccall "joinMulticast" c_joinMulticast :: CInt -> CInt -> CInt -> IO CInt
foreign import ccall "leaveMulticast" c_leaveMulticast :: CInt -> CInt -> CInt -> IO CInt



getMulticastOrigin ip = do
    let pip = parseIP ip
    receiveSocket <- socket AF_INET Datagram defaultProtocol
    setSocketOption receiveSocket ReuseAddr (-1)
    enableReusePort receiveSocket
    bindSocket receiveSocket (SockAddrInet (PortNum $ swapBytes (cast $ cfgPort config)) 0 {-0x0100007F-})
    let fd = cast $ fdSocket receiveSocket
    joinMulticast fd (cast pip) (cast $ parseIP (cfgBindInterface config))
    ptr <- mallocBytes 1500
    h <- socketToHandle  receiveSocket ReadMode
    putStrLn "begin hWaitForInput"
    avail <- hWaitForInput h 5000
    putStrLn $ "end hWaitForInput: data avail:" ++ show avail
    maybeData <- if avail then do
                -- putStrLn "begin recvFrom"
                    retval <- timeout 1000000 (recvBufFrom receiveSocket ptr 1500)
                -- putStrLn $ "end recvFrom: "++(show retval)
                    return $ fromMaybe (0,undefined) retval
                  else return (0,undefined)
    free ptr
    hClose h
    case maybeData of
        (0,_) -> do
            putStrLn $ "ERROR: cannot find reverse mapping of "++(ip) ++ " (timeout waiting for packets)"
            return Nothing
        (_,addr) -> do
            putStrLn $ "reverse mapping of "++(ip)++" = "++(show addr)
            return $ Just addr

{-
instance Ord SockAddr where
            compare (SockAddrInet pn1 ha1) (SockAddrInet pn2 ha2) = compare (pn1,ha1) (pn2,ha2)
            compare _ _ = EQ
-}

captureImmediately = do
    putStrLn "begin immediate capture to file."
    receiveSocket <- socket AF_INET Datagram defaultProtocol
    setSocketOption receiveSocket ReuseAddr (-1)
    enableReusePort receiveSocket
    bindSocket receiveSocket (SockAddrInet (PortNum $ swapBytes (cast $ cfgPort config)) 0)
    let fd = cast $ fdSocket receiveSocket
    let multicastIp = parseIP $ cfgMulticastAddr config
    joinMulticast fd (cast multicastIp) (cast $ parseIP (cfgBindInterface config))
    outf <- openBinaryFile (cfgCaptureImmediateFileOnce config) WriteMode
    chan <- newChan :: IO (Chan ToSave)
    forkIO $ forever $ do
        (ToSave time pkts) <- readChan chan
        writeToFile outf time pkts
        putStr "w"
        hFlush stdout
    forever $ do
        ptr <- mallocBytes 1500
        (cnt,_) <- recvBufFrom receiveSocket ptr 1500
        time <- getTime Realtime
        if (cnt == 7 * 188) then do
            putStr "<"
            hFlush stdout
            writeChan chan $ ToSave time ptr
          else
            putStrLn "error packet received"

runServer = do
    when (cfgDaysToKeep config > 0) $
        (forkIO $ cleanupper (cfgSpoolDirectory config) (cfgDaysToKeep config)) >> return ()
    print config
    if (cfgCaptureImmediateFileOnce config /= []) then do
        captureImmediately
      else do
        let ips = splitRegex (mkRegex ",") $ cfgMulticastAddr config
        originAndIp <- catMaybes <$> mapM (\ip -> do
            -- here the real source address of feeder is calculated
            origin <- getMulticastOrigin ip
            return $ fmap (flip (,) ip) origin
            ) ips
        print "done with resolving"
        receiveSocket <- socket AF_INET Datagram defaultProtocol
        setSocketOption receiveSocket ReuseAddr (-1)
        enableReusePort receiveSocket
        bindSocket receiveSocket (SockAddrInet (PortNum $ swapBytes (cast $ cfgPort config)) 0)
        let fd = cast $ fdSocket receiveSocket
        channelMapVar <- newMVar $ M.empty
        channelMap <- M.fromList <$> mapM (\(origin,ip) -> do
            putStrLn $ "Joining multicast:: "++ip
            let multicastIp = parseIP ip
            let jm = joinMulticast fd (cast multicastIp) (cast $ parseIP (cfgBindInterface config))
            let lm = leaveMulticast fd (cast multicastIp) (cast $ parseIP (cfgBindInterface config))
            result <- jm
            putStrLn $ "  --> "++(show result)
            nChan <- mkChannel
            channel <- newMVar nChan
            let dirName = (cfgSpoolDirectory config) ++ "/"++ip
            createDirectoryIfMissing True dirName
            let storage = ITSStorage dirName
            currentSegment <- newIORef Nothing
            let rejoin = do
                lm
                newOrigin <- getMulticastOrigin ip
                withMaybe newOrigin $ \norigin -> do
                    oldMap <- takeMVar channelMapVar
                    let newMap = M.insert norigin channel oldMap
                    putMVar channelMapVar newMap
                jm
                return ()
            forkIO $ saver nChan channel currentSegment storage ip (rejoin)
            return (origin,channel) ) originAndIp
        swapMVar channelMapVar channelMap
        forkIO $ receiver channelMapVar receiveSocket
        forever $ threadDelay 1000000
        return ()

writeToFile h (TimeSpec a b) dta = do
    alloca $ \aptr -> do
    alloca $ \bptr -> do
        poke aptr a
        poke bptr b
        hPutBuf h aptr 4
        hPutBuf h bptr 4
        hPutBuf h dta $ 188*7


readChanFully chan acc = do
    wasEmpty <- isEmptyChan chan
    if (wasEmpty) then return $ reverse acc
        else do
            v <- readChan chan
            readChanFully chan (v:acc)

main = runServer

