module Utils.SanUtils where

import System.IO.Unsafe
-- import System.Posix.Clock
import Foreign.ForeignPtr
import Foreign.Storable
import Foreign.Ptr
import Data.Bits
import Data.Word
import Data.Maybe
import Data.ByteString.Internal
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Debug.Trace
import Text.Regex
import Data.Time.Clock.POSIX
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Language
import Text.Parsec.Token
import Control.Applicative

-- import Data.Time.Clock
-- import Data.Time.Format

isEmptyLine :: ByteString -> Bool
isEmptyLine s =
        let ss = map w2c $ BS.unpack s
        in BS.length s <= 2 && (ss == "\r" || ss == "\r\n")

fromRight (Right x) = x
fromRight _ = error "fromRight <- left"
fromLeft (Left x) = x
fromLeft _ = error "fromLeft <- right"
isRight (Right _) = True
isRight _ = False

traceval a = trace (show a) a
_traceval a = a
traceval1 l a = trace (l ++ (show a)) a
_traceval1 _ a = a
_trace _ b = b

expect nm h check after = do
        str <- BS.hGetLine h
        putStrLn $ "expect got ("++nm++"): `"++(map w2c $ BS.unpack str)++"`"
        if (check str)
            then do
                -- putStrLn "expect returns Just"
                after str >>= return . Just
            else do
                putStrLn $ "check failed ("++nm++"): `"++(map w2c $ BS.unpack str)++"`"
                --hPutStrLn h "sorry bye"
                return Nothing

fromJustX :: String -> Maybe a -> a
fromJustX _ (Just x) = x
fromJustX lbl Nothing = error $ "fromJustX: Nothing, label=" ++ lbl

swapBytes i =
        let lo = (i .&. 0xFF) `shiftL` 8
            hi = i `shiftR` 8
        in lo .|. hi


fromMaybeNVL (Just x) _ = x
fromMaybeNVL (Nothing) n = n

currentTimeMillis :: IO Word64
currentTimeMillis = do
    pt <- getPOSIXTime
    return $ round $ 1000 * (fromRational $ toRational pt :: Double)

currentTimeMillisF = do
    pt <- getPOSIXTime
    return $ (fromRational $ toRational pt :: Double)
    {-
        (TimeSpec a b) <- getTime Realtime
        let millis = a * 1000 + (b `div` 1000000)
        return $ (fromIntegral millis) / (1000.0 :: Double)
	-}

word32ToBytes :: Word32 -> [Word8]
word32ToBytes w32 =
    map (toEnum . fromEnum) $ [w32 `shiftR` 24, (w32 `shiftR` 16 .&. 0xFF), (w32 `shiftR` 8 .&. 0xFF), (w32 .&. 0xFF)]

word32ToBytesPtr :: Word32 -> Ptr Word8 -> IO ()
word32ToBytesPtr w32 ptr = do
    poke ptr $ cast $ w32 `shiftR` 24
    poke (ptr `plusPtr` 1) $ cast $ w32 `shiftR` 16 .&. 0xFF
    poke (ptr `plusPtr` 2) $ cast $ w32 `shiftR` 8 .&. 0xFF
    poke (ptr `plusPtr` 3) $ cast $ w32 .&. 0xFF
    where cast = toEnum . fromEnum

word16ToBytes :: Word16 -> [Word8]
word16ToBytes w16 =
    map (toEnum . fromEnum) $ [(w16 `shiftR` 8 .&. 0xFF), (w16 .&. 0xFF)]

word16ToBytesPtr :: Word16 -> Ptr Word8 -> IO ()
word16ToBytesPtr w16 ptr = do
    poke ptr $ cast (w16 `shiftR` 8 .&. 0xFF)
    poke (ptr `plusPtr` 1) $ cast (w16 .&. 0xFF)
    where cast = toEnum . fromEnum


renum a =
    let len = fromIntegral $ length a
    in zip a ([i / len |i <- [0..(len-1)]] :: [Double])

headX label_ [] = error $ "head: "++label_
headX _ s = head s


withBSAsPtr bs act = unsafePerformIO $ do
    let [chunk] = BSL.toChunks bs
    let (fp, 0, _) = toForeignPtr chunk
    withForeignPtr fp $ \ptr -> do
        return $ act (ptr) fp


ltrim s = dropWhile (==' ') s
trim s = reverse $ ltrim $ reverse $ ltrim s
endsWith p s = (length p <= length s) && p == (reverse $ take (length p) $ reverse s)

startsWith (i:is) (a:as) = a == i && startsWith is as
startsWith [] _ = True
startsWith _ _ = False

containsText pat text =
        isJust $ matchRegex (mkRegex pat) text

safeHead [] = Nothing
safeHead (x:_) = Just x

withMaybe (Just a) f = f a
withMaybe (Nothing) _ = return ()

parseIP :: String -> Word32
parseIP str =
    let p = do
        p1 <- fromInteger <$> integer haskell
        char '.'
        p2 <- fromInteger <$> integer haskell
        char '.'
        p3 <- fromInteger <$> integer haskell
        char '.'
        p4 <- fromInteger <$> integer haskell
        return $ p4 `shiftL` 24 + p3 `shiftL` 16 + p2 `shiftL` 8 + p1
    in case (runParser p () "ip address" str) of
                Left err -> error $ show err
                Right r -> r


