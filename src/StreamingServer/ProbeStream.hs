{-# OPTIONS -XDeriveDataTypeable #-}
module Main where

import Control.Monad
import System.IO.Unsafe
import RelayingServer.Relaying
import System.Console.CmdArgs
import MPEG.Server

data Config = Config { probeURL :: String,
                        delaySeconds :: Int
                         } deriving (Show, Data, Typeable)

config  = unsafePerformIO $ cmdArgsRun $ cmdArgsMode $ Config {
                                                probeURL = ""  &= help "http:// or udp:// url" &= name "probeURL" &= explicit,
                                                delaySeconds = 3 &= help "stop probing after specified number of seconds [1..10], default 3" &= name "delaySeconds" &= explicit
                                                } &= summary  "IPTV http/multicast prober, version 0.1e-235\nPowered by Haskell."
                                                  &= program "probe_stream"


main = do
    let url = probeURL config
        bufSize = 100000
    when (url == "") $ fail "missing url"
    buffer <- mkCircularBuffer bufSize
    let seconds = if delaySeconds config > 10 then 10 else if delaySeconds config < 0 then 1 else delaySeconds config
    ids <- probeStream url buffer seconds
    mapM (\i -> putStrLn $ "RESULT:ID:"++(show i)) ids
    print "DONE"
