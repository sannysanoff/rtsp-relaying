{-# Options -XDeriveDataTypeable #-}

module Main where
-- vim: sw=4:ts=4:expandtab

import MPEG.MpegTs
import System.Console.CmdArgs
import System.IO.Unsafe

import System.IO
import System.IO.Error
--import System.Time
-- import qualified Data.ByteString as S
import Control.Monad
import Control.Concurrent
import Network.Socket

import MPEG.MpegTypes
import Utils.SanUtils
import MPEG.Server


-- import System.Posix.Types


data Config = Config {  -- cfgBindInterface :: String,
                        cfgPort :: Int,
                        cfgReplayDirectory :: String } deriving (Show, Data, Typeable)

{-- Dies ist eine globale Variable!!!!! -}
config = unsafePerformIO $ cmdArgsRun $ cmdArgsMode $ Config {
                                                cfgPort = (8554 :: Int) &= help "rtsp control interface port" &= name "p",
                                                cfgReplayDirectory = "/home/spool/iptv-save" &= help "directory for spool files" &= name "d"
                                                } &= summary "IPTV play/replay, version 0.1e-236"


defaultPortNumber = toEnum $ cfgPort config

streamingServer = do
    srvr <- mkRTSPServer
    serverSocket <- socket AF_INET Stream defaultProtocol
    setSocketOption serverSocket ReuseAddr (-1)
    bindSocket serverSocket (SockAddrInet (PortNum $ swapBytes defaultPortNumber) 0)
    listen serverSocket 200
    putStrLn $ "URLS are in form : rtsp://127.0.0.1:"++(show defaultPortNumber)++"/filename.ts"
    putStrLn $ "Storage is       : " ++ (cfgReplayDirectory config)
    forever $ do
        (sock, addr) <- accept serverSocket
        -- putStrLn $ "connection from: "++(show addr)
        forkIO $ do
            h <- socketToHandle sock ReadWriteMode
            catchIOError (serveRTSP h h addr srvr :: IO ()) (\err -> print $ "serve rtsp/tcp: error" ++ (show err) )
            hClose h
            return ()

serveRTSP :: Handle -> Handle -> SockAddr -> RTSPServer -> IO ()
serveRTSP h ho sa srvr = do
    cleanup <- newMVar []
    catchIOError (serveRTSP' h ho sa srvr cleanup $ cfgReplayDirectory config) (\e -> print e)
    releasers <- takeMVar cleanup
    sequence releasers
    return ()


main = do
    streamingServer
    -- print "hola!"
