-- {-# OPTIONS -fglasgow-exts #-}
{-# LANGUAGE ForeignFunctionInterface #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE MagicHash #-}
{-# OPTIONS -fexcess-precision #-}







--- iso13818-1.pdf was used to work with MPEG-TS format





module Codec.MpegTS.Formats where

import Data.Bits
import Data.Maybe
import Data.List
import GHC.Word
import GHC.Ptr
import GHC.Base
import Data.ByteString.Internal
import qualified Data.ByteString.Lazy as BS
-- import Debug.Trace
import Text.Parsec.Prim
import Text.Parsec.Char
import Text.Parsec.Combinator
import Text.Parsec.Language
import Text.Parsec.Token
import qualified Data.Foldable as F

import MPEG.MpegTypes
import Debug.Trace
import Utils.SanUtils
import Numeric(showHex)
import Foreign

import System.IO



{-# INLINE peek0 #-}
peek0 (Ptr p) = W8# (indexWord8OffAddr# p 0#)

{-# INLINE peekn #-}
peekn (I# n) (Ptr p) = W8# (indexWord8OffAddr# p n)

-- getting right timestamp (from right kind of packets)
getPacketTS (TSPacket { tspPES = Nothing }) = Nothing
getPacketTS pkt@(TSPacket { tspPES = Just pes }) =
    let dts = pesGetDTS' pkt
        pts = pesGetPTS' pkt
        ok = isVideo $ pesStreamID pes
        frameType = pesFrameType pes
    in
        case (ok, dts,pts,frameType) of
        (True, Nothing, Just p, _) -> Just p
        (True, Just d, Just p, Just BFrame) -> if p == 0 && d > p then Just d else Just p
        _ -> Nothing

-- packet is after specified time "u" ?
packetIsPast u pkt = F.or $ fmap (>= u) $ getPacketTS pkt

dump ptr =
    let valz = intercalate "," $ map (\i -> showHex (peekn i ptr) "") [0..10]
    in "["++valz++"]"

decodePMT ptr_ =
    let         -- (see Table 2-28)
        ptr = ptr_ `plusPtr` 1     --
        table_id = fromEnum $ peek0 ptr            -- starts at byte 4 in whole TS packet (without adapt field)
        b1 = peekn 1 ptr
        b2 = peekn 2 ptr        -- (section_syntax_indicator(1),'0',reserved(2),section_len(12))
        section_len = (((fromEnum b1) `shiftL` 8) .|. (fromEnum b2)) .&. 0x0FFF
        pn1 = fromEnum $ peekn 3 ptr
        pn2 = fromEnum $ peekn 4 ptr
        programNumber = (pn1 `shiftL` 8) .|. pn2
        _ = peekn 5 ptr   -- (reserved(2),version(5),current_next(1))
        -- section_number = peekn 6 ptr
        -- last_section_number = peekn 7 ptr               -- 8 bytes so far
        pcrpid1 = fromEnum $ peekn 8 ptr
        pcrpid2 = fromEnum $ peekn 9 ptr
        pcrPid = ((pcrpid1 `shiftL` 8) .|. pcrpid2) .&. 0x1FFF
        pil1 = fromEnum $ peekn 10 ptr
        pil2 = fromEnum $ peekn 11 ptr                  -- 12 bytes so far
        program_info_length = ((pil1 `shiftL` 8) .|. pil2) .&. 0xFFF
        progrsStart = 12
        progs = reverse $ decodePrograms (section_len - progrsStart - program_info_length - 1, ptr `plusPtr` (progrsStart + program_info_length))
    in Just $ PMTPacket {
            pmtTableId = table_id,
            pmtPCR_PID = pcrPid,
            pmtProgramNumber = programNumber,
            pmtStreams = progs
        }
    where
        decodePrograms :: (Int, Ptr Word8) -> [PMTStream]
        decodePrograms (0,_) = []
        decodePrograms (remaining,ptr)
            | remaining <= 0  = []
            | otherwise =
            let
                streamType = fromEnum $ peek0 ptr
                pid1 = fromEnum $ peekn 1 ptr
                pid2 = fromEnum $ peekn 2 ptr
                pid = ((pid1 `shiftL` 8) .|. pid2) .&. 0x1FFF
                infol1 = fromEnum $ peekn 3 ptr
                infol2 = fromEnum $ peekn 4 ptr
                infol = ((infol1 `shiftL` 8) .|. infol2) .&. 0xFFF
            in
                PMTStream { pmtsStreamType = streamType, pmtsPID = pid} :
                    decodePrograms (remaining - 5 - infol, ptr `plusPtr` (5 + infol))


decodePAT _ ptr_ _ =
    let         -- see table 2-25
        pointer = peek0 ptr_
        ptr = ptr_ `plusPtr` (1 + fromEnum pointer)
        table_id = peek0 ptr            -- starts at byte 4 in whole TS packet (without adapt field)
        b1 = peekn 1 ptr
        b2 = peekn 2 ptr        -- (section_syntax_indicator(1),'0',reserved(2),section_len(12))
        tsid1 = peekn 3 ptr
        tsid2 = peekn 4 ptr     -- transport_stream_id (16)
        vercn = peekn 5 ptr     -- (reserved(2), version(5), current_next(1))
        section_number = peekn 6 ptr
        last_section_number = peekn 7 ptr               -- end of 8 byte header
        -- section_syntax_indicator = b1 `shiftR` 7
        -- mustbe0 = (b1 `shiftR` 6) .&. 1
        section_len = (((fromEnum b1) `shiftL` 8) .|. (fromEnum b2)) .&. 0x0FFF
        programsStart = ptr `plusPtr` 8
        programs = map (\i ->
            let programPtr = programsStart `plusPtr` (i * 4)
                pn1 = peekn 0 programPtr
                pn2 = peekn 1 programPtr
                pid1 = peekn 2 programPtr
                pid2 = peekn 3 programPtr
                program_no = ((fromEnum pn1) `shiftL` 8) .|. (fromEnum pn2)
                pid = (((fromEnum pid1) `shiftL` 8) .|. (fromEnum pid2)) .&. 0x1FFF
            in if program_no == 0 then Left pid else Right pid
           ) [0..(section_len-9) `div` 4 - 1]
    in Just $ PATPacket {
        patTableId = fromEnum table_id,
        patTransportStreamId = (((fromEnum tsid1) `shiftL` 8) .|. (fromEnum tsid2)),
        patCurrentOrNext = vercn .&. 1 == 1,     -- current = true
        patSectionNumber = fromEnum section_number,
        patLastSectionNumber = fromEnum last_section_number,
        patPrograms = programs
    }

decodePES fileOffset ptr remains
  | remains > 20 = _traceval1 "PES=" $ decodePESPtr ptr
  | otherwise = {- trace ("nothing2 @ " ++(show fileOffset)) $ -} Nothing
     where decodePESPtr pesData =       -- see (table 2-17) or maybe PMT
                let b1 = fromEnum $ peek0 pesData          -- packet_start_code_prefix
                    b2 = fromEnum $ peekn 1 pesData
                    b3 = fromEnum $ peekn 2 pesData
                    streamId = peekn 3 pesData  -- stream_id
                    mp = peekn 5 pesData
                    chk = peekn 6 pesData
                    ind0 = peekn 7 pesData
                    p2 = pesData `plusPtr` 9
                    checkData = (chk `shiftR` 6,
                                    (b1 `shiftL` 16) .|. (b2 `shiftL` 8) .|. b3,
                                    (b1 `shiftL` 8) .|. b2        -- '00 NN' (nn = table number)
                                    )
                in _trace ("checkData="++(show checkData)) $ case checkData of
                (0x02, 1, _) ->        -- (binary '10' = first case,   1 = packet_start_code_prefix '00 00 01')
                    let
                                ind0' = ind0 `shiftR` 6
                                (dts,pts) = case ind0' of
                                        3 -> (Just $ mkFloatP (p2 `plusPtr` 5), Just $ mkFloatP p2)
                                        2 -> (Nothing, Just $ mkFloatP p2)
                                        _ -> (Nothing,Nothing)
                    in _trace "PES OK 1" $
                        if (isVideo streamId || isAudio streamId) then
                            let subsequent = _traceval1 (show ("subsequent",dts,pts,dump (ptr `plusPtr` 19))) $
                                        decodePES fileOffset (ptr `plusPtr` 19) (remains - 19)
                                retval = _traceval1 ("pespkt at "++(show (fileOffset,streamId, isPictureHeader streamId,mp))) $
                                        Just PESPacket {
                                pesDTS = dts,
                                pesPTS = pts,
                                pesStreamID = fromEnum streamId,
                                pesInd0 = fromEnum $ ind0',
                                pesFrameType = case subsequent of
                                                Just PESPacket { pesStreamID = 0, pesFrameType = x } -> x
                                                _ -> Nothing
                                 }
                            in retval
                          else if (isPictureHeader streamId) then
                              Just PESPacket {
                                pesDTS = dts,
                                pesPTS = pts,
                                pesStreamID = fromEnum streamId,
                                pesInd0 = fromEnum $ ind0',
                                pesFrameType = decodeFrameType $ mp `shiftR` 3 .&. 3
                                 }
                          else Nothing
                (_, _, 0x0002) -> -- Program Map Table  (pointer field, also see 2.4.4.9, Table 2-28)
                        decodePMT pesData
                _ -> {-trace ("bad case PES: " ++ show (x,"offset",fileOffset)) $ -}Nothing

decodeFrameType 3 = Just BFrame
decodeFrameType 2 = Just PFrame
decodeFrameType x = trace ("oops" ++ show x) $ Nothing

isVideo sid
    | sid >= 0xE0 && sid <= 0xEF = True
    | otherwise = False

isAudio sid
    | sid == 0xFD  = True
    | otherwise = False

isPictureHeader sid = sid == 0

mkFloat (w1:w2:w3:w4:w5:_) =
        let hi3 = fromEnum $ (w1 `shiftR` 1 .&. 0x7)
            med1_8 = fromEnum $ w2
            med2_7 = fromEnum $ (w3  `shiftR` 1 .&. 0x7F)
            lo1_8 = fromEnum w4
            lo2_7 = fromEnum (w5 `shiftR` 1 .&. 0x7F)
            lo15 = lo2_7 .|. (lo1_8 `shiftL` 7)
            med15 = med2_7 .|. (med1_8 `shiftL` 7)
            whole = lo15 .|. (med15 `shiftL` 15) .|. (hi3 `shiftL` 30)
        in (fromIntegral $ whole) / 90000.0
mkFloat _ = error "mkfloat not enough values"

mkFloatP ptr =
        let (w1,w2,w3,w4,w5) = (peek0 ptr,peekn 1 ptr, peekn 2 ptr, peekn 3 ptr, peekn 4 ptr)
            hi3 = fromEnum $ (w1 `shiftR` 1 .&. 0x7)
            med1_8 = fromEnum $ w2
            med2_7 = fromEnum $ (w3  `shiftR` 1 .&. 0x7F)
            lo1_8 = fromEnum w4
            lo2_7 = fromEnum (w5 `shiftR` 1 .&. 0x7F)
            lo15 = lo2_7 .|. (lo1_8 `shiftL` 7)
            med15 = med2_7 .|. (med1_8 `shiftL` 7)
            whole = lo15 .|. (med15 `shiftL` 15) .|. (hi3 `shiftL` 30)
        in (fromIntegral $ whole) / 90000.0


decodePacket :: Ptr Word8 -> Ptr Word8 -> Int -> Maybe TSPacket
decodePacket ptr savePtr offset =   -- see table 2-2
            let !b1 = peek0 ptr     -- sync_byte
                !b2= peekn 1 ptr    -- part1
                !b3 = peekn 2 ptr   -- part2 of (transport_error_indicator(1), payload_unit_start_indicator(1),
                                    --            transport_priority(1),PID(13)
                -- !payload_unit_start_indicator = (b2 `shiftR` 6) .&. 1
                !b4= peekn 3 ptr    -- transport_scrambling_control(2),adaptation_field_control(2),continuity_counter(4)
                !b5= peekn 4 ptr    -- adaptation_field_length (if exists)
                !maybeAdaptationFieldLength = if adaptationFieldControl .&. 2 /= 0
                    then b5+1       -- adaptation field length, including length field itself, so this is total size of adaptation field
                    else 0
                !adaptationFieldControl = (b4 `shiftR` 4) .&. 0x3
                !dataStart = fromEnum $ 4 + maybeAdaptationFieldLength
                !pesPacket = if adaptationFieldControl .&. 1 /= 0    -- some data here
                               then
                                    case pid of
                                        0 -> decodePAT offset (ptr `plusPtr` dataStart) (188 - (toEnum dataStart))
                                        _ -> decodePES offset (ptr `plusPtr` dataStart) (188 - (toEnum dataStart))
                               else Nothing
                !pid = ((fromEnum b2 .&. 0x1F) `shiftL` 8) .|. (fromEnum b3)
            in case (b1) of
                (0x47) -> Just TSPacket {
                                        tspPID = pid,
                                        tspPES = pesPacket,
                                        tspFileOffset = offset,
                                        tspDataPtr = savePtr -- fptr0
                                  }
                (_) -> trace ("bad packet at offset "++(show offset)) Nothing

-- Transport: RTP/AVP;unicast;client_port=56496-56497

decodeTransport bsval =
        let p = elementP `sepBy` (char ';')
            val = map w2c $ BS.unpack bsval
            elementP = paRTPAVP <|> paUnicast <|> paClientPort <|> paModePlay
            paRTPAVP = string "RTP/AVP" >> (return $ TWOther "RTP/AVP")
            paUnicast = string "unicast" >> (return TWUnicast)
            paModePlay = string "mode=\"PLAY\"" >> (return TWModePlay)
            paClientPort = do
                        string "client_port="
                        from <- natural haskell
                        char '-'
                        to <- natural haskell
                        return $ TWClientPort (fromInteger from) (fromInteger to)
        in case (runParser p () "transport" val) of
                Left err -> [TWError $ show err ]
                Right r -> r




decodeHeader :: ByteString -> Either String (String,String)
decodeHeader s =
        let p = do
                headerName <- manyTill (letter <|> digit <|> char '-') (char ':')
                spaces
                headerValue <- optionMaybe $ manyTill anyChar (char '\n' <|> char '\r')
                return $ Right (headerName, fromMaybe "" headerValue)
        in case (runParser p () "location" s) of
                Left err -> traceval1 ("decodeHeader("++(show s)++"): ") $ Left $ show err
                Right val -> val



min2 (Just a) (Just b) = min a b
min2 Nothing (Just b) = b
min2 (Just a) Nothing = a
min2 Nothing Nothing = error "max2: oops!"


-- various extractors
pesGetDTS (TSPacket { tspPES = Just (PESPacket { pesDTS = Just dts  })}) = dts
pesGetDTS _ = error "getPacketDTS error"
pesGetDTS' (TSPacket { tspPES = Just (PESPacket { pesDTS = Just dts  })}) = Just dts
pesGetDTS' _ = Nothing
pesGetPTS (TSPacket { tspPES = Just (PESPacket { pesPTS = Just pts  })}) = pts
pesGetPTS _ = error "getPacketPTS error"
pesGetPTS' (TSPacket { tspPES = Just (PESPacket { pesPTS = Just pts  })}) = Just pts
pesGetPTS' _ = Nothing

-- streamTypeVideo :: Integral a => a
-- streamTypeVideo = 0xE0stream-00000738
pesGetStreamType (TSPacket { tspPES = Just (PESPacket { pesStreamID = streamId  })}) = streamId
pesGetStreamType _ = -1


sampleDecode =
    withBinaryFile "/home/san/iptv/analyze/stream-0000073A" ReadMode $ \h -> do
        allocaBytes 1000 $ \ptr -> do
            hGetBuf h ptr 1000
            let wptr = ptr `plusPtr` 188
            let pkt = decodePacket wptr wptr 0
            print pkt
            return ()
