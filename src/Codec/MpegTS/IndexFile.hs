module Codec.MpegTS.IndexFile where

import System.IO
import System.Locale
import Data.Time.Clock
import Data.Time.Format
import qualified Data.ByteString.Lazy as BS
--import Control.Concurrent.MVar
import System.Directory
import System.IO.MMap
import Control.Concurrent
import Foreign
import Debug.Trace

import MPEG.MpegTypes
import Codec.MpegTS.Formats
import Utils.SanUtils

showTime ::  FormatTime t => t -> String
showTime t =
    formatTime defaultTimeLocale "%T" t


maybeStartIndexing fpath = do
    ok <- doesFileExist fpath
    okindex <- doesFileExist $ fpath++".index"
    okindex1 <- doesFileExist $ fpath++".index.tmp"
    print ("maybeStartIndexing",fpath,ok,okindex,okindex1)
    if (ok) then
        if (okindex || okindex1) then return ()
          else forkIO (startIndexing fpath) >> return ()
      else return ()

dummyInitialPts = -999999.0

startIndexing fpath = do
    tm1 <- getCurrentTime
    putStrLn $ (showTime tm1) ++ ": start indexing file: "++fpath
    withBinaryFile fpath ReadMode $ \ih -> do
        withBinaryFile (fpath ++ ".index.tmp") WriteMode $ \oh -> do
            hSetBuffering oh (BlockBuffering Nothing)
            -- hSeek ih AbsoluteSeek 726584092
            scanVideoFrames ih 0 dummyInitialPts $ \(offs, pts) -> do
--                        let offsb = encode (offs::Int)
--                            ptsb = encode (pts::Double)
                -- print (offs,pts)
                writeStorable oh pts
                writeStorable oh offs
--                        BS.hPut oh ptsb
--                        print ("length ptsb",BS.length ptsb)
--                        BS.hPut oh offsb
--                        print ("length offsb",BS.length offsb)
    renameFile (fpath ++ ".index.tmp") (fpath ++ ".index")
    tm2 <- getCurrentTime
    putStrLn $ (showTime tm2) ++ ": stop indexing file: "++fpath

scanVideoFrames :: Handle -> Int -> Double -> ((Int, Double) -> IO ()) -> IO ()
scanVideoFrames ih offs previousPts handler = do
    fpos <- hTell ih
    bs <- BS.hGet ih 188
    -- print $ ("svf",offs, previousPts)
    if (BS.length bs == 188) then
        withBSAsPtr bs $ \ptr _ ->
            case decodePacket ptr nullPtr offs of
                Just pkt -> do
                    npts <- case pesGetPTS' pkt of
                                Just pts -> do if (pts > previousPts && pts - previousPts < 2 || dummyInitialPts == dummyInitialPts)
                                                    then do
                                                            -- print $ "call handler @"++show (fpos,offs, pts)
                                                            handler (offs, pts) >> return pts
                                                    else trace ("pts < prevPts: " ++ show  (pts, previousPts)) $
                                                            return previousPts
                                Nothing -> {- trace "no pts!" $ -}
                                            return previousPts
                    scanVideoFrames ih (offs+1) npts handler
                Nothing -> do
                    print $ "can't decode packet pos="++show (fpos)
                    return ()
      else do
        print $ "short read, "++show ( BS.length bs, fpos)
        return ()

indexEntrySize = sizeOf (undefined :: Double) + sizeOf (undefined :: Int)

openIndex fpath = do
    (fptr, offs, rawSize) <- mmapFileForeignPtr fpath ReadOnly Nothing
    return $ OpenIndex (castForeignPtr fptr) offs rawSize

closeIndex (OpenIndex fptr _ _) = do
    finalizeForeignPtr fptr

getIndexSize (OpenIndex _ _ rawSize) = rawSize `div` indexEntrySize

getIndexEntry  i ix@(OpenIndex fp offs rawSize) =
    if (i < 0 || i >= getIndexSize ix)
        then error $ " getIndexEntry out of bounds: rawSize="++(show rawSize)++" i="++(show i)
        else withForeignPtr fp $ \ptr ->
            do
                let ppts = plusPtr ptr $ offs + i * indexEntrySize
                    poffs = plusPtr ptr $ offs + i * indexEntrySize + sizeOf (undefined :: Double)
                pts <- peek (castPtr ppts) :: IO Double
                fpos <- peek (castPtr poffs) :: IO Int
                return (pts, fpos)

substring from to =
    BS.take (to - from) . BS.drop from

writeStorable h a = do
  alloca $ \p -> do
      poke p a
      hPutBuf h p (sizeOf a)

readStorable h a = do
  alloca $ \p -> do
          hGetBuf h p (sizeOf a)
          peek p

{--
storableCast :: (Storable a, Storable b) => a -> b
storableCast x
  | otherwise = let b = unsafePerformIO $
                      alloca $ \p -> do
                        poke p x
                        peek $ castPtr p
                in if (sizeOf x /= sizeOf b) then error "storableCast: sizes do not match"
                       else b

--}
