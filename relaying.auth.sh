#set -x
CLIENT_IP="$1"
AUTH_SIG="$2"
USER_AGENT="$3"
START_STOP="$4"

if [ "$START_STOP" = "1" ] ; then
# client connected
	if [ "$USER_AGENT" = "ABCDE" ] ; then
		echo "NoAdvertisement"
	else
		echo "DisplayInternalError"
	fi
else
# client disconnected, need to return something
	echo "NoAdvertisement"
fi
